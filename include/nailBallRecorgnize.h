#pragma once

#include "global.h"

/** 使用流程：
* 获取初始术中2D矩阵并显示，调用追踪钉投影圆心识别，
* ①如果成功，交互输入 追踪钉尖端坐标
* ②如果失败，交互输入 投影圆心，投影圆半径，追踪钉尖端坐标
* 输入物理球半径，调用手动计算更新函数，获取像素间距，最终术中2D矩阵，最终钉3D坐标和位置矩阵
* 传入 最终术前3D矩阵，获取追踪钉和3D椎骨的相对位置矩阵
*/

class PoseCalculator : public QObject
{
    Q_OBJECT

public:
    PoseCalculator();
    ~PoseCalculator();

private:
    using PixelType = signed short;
    using DcmImage2DType = itk::Image< PixelType, 2U >;
    using DcmImage3DType = itk::Image< PixelType, 3U >;

    using Point2DType = itk::Point<double, 2U>;
    using Point3DType = itk::Point<double, 3U>;

    using Matrix4x4Type = vtkMatrix4x4;

public:

    Q_INVOKABLE void setNailBallPhysicalRadius(const qreal radiusInPhysical);

    Q_INVOKABLE void setInitCoronalOrigin(const qreal origin0,const qreal origin1);
    Q_INVOKABLE void setInitSagittalOrigin(const qreal origin0,const qreal origin1);

    Q_INVOKABLE void setInitCoronalSpacing(const qreal spacing0,const qreal spacing1);
    Q_INVOKABLE void setInitSagittalSpacing(const qreal spacing0,const qreal spacing1);
  
    //只旋转过后垂直角度的初始位置 //只用于术中2D
   Q_INVOKABLE QVariantList getInitCoronalMatrix();
   Q_INVOKABLE QVariantList getInitSagittalMatrix();

    Q_INVOKABLE QVariantList getInitProjectedCoronalMatrix();
    Q_INVOKABLE QVariantList getInitProjectedSagittalMatrix();

    //设置追踪钉尖端的vtk坐标
    Q_INVOKABLE void setCoronalPeakPoints( const QVariantList& peakPointInCoronal);
    Q_INVOKABLE void setSagittalPeakPoints( const QVariantList& peakPointInSagittal);

    Q_INVOKABLE qreal getNewSpacingOfCoronal();
    Q_INVOKABLE qreal getNewSpacingOfSagittal();

    //重新设置像素间距后的圆心 
    Q_INVOKABLE QVariantList getRespacingCircleCenterInCoronal();
    Q_INVOKABLE QVariantList getRespacingCircleCenterInSagittal();

    //重新设置像素间距后的尖端
    Q_INVOKABLE QVariantList getRespacingPeakPointInCoronal();
    Q_INVOKABLE QVariantList getRespacingPeakPointInSagittal();

    //传入qml识别的半径 单位mm 
    Q_INVOKABLE void setCoronalBallRadius(const qreal nailCoronalRadius);
    Q_INVOKABLE void setSagittalBallRadius(const qreal nailsagittalRadius);

    //传入 圆心
    Q_INVOKABLE void setCoronalBallCenter(
         const qreal coronalCircleCenterX,const qreal coronalCircleCenterY);

    Q_INVOKABLE void setSagittalBallCenter(
              const qreal sagittalCircleCenterX,const qreal sagittalCircleCenterY);

    Q_INVOKABLE void calculateUseInteractData();//用交互数据进行计算

    Q_INVOKABLE QVariantList getUpdatedCoronalMatrix();
    Q_INVOKABLE QVariantList getUpdatedSagittalMatrix();
    //std::array<Matrix4x4Type, 2> getInitMatrixOfCoronalAndSagittal();

    //放缩因子，用于将正侧位片上没有设置新spacing时的点坐标变换为spacing后的对应位置
    Q_INVOKABLE qreal getSaclarFactorSagittal();
    Q_INVOKABLE qreal getSaclarFactorCoronal();
    //术中正侧位片 Z方向差值 （已经spacing后的距离）
    Q_INVOKABLE qreal getIntra2DImageDiffAlongZ();

    //缩放后的追踪钉特征3D
    Q_INVOKABLE QVariantList getRespacingNailCenterIn3D();
    Q_INVOKABLE QVariantList getRespacingNailPeakIn3D();
     //粗配 术中正ce位片选点
     Q_INVOKABLE void setRGPInIntraCoronal(const QVariantList& coronalPoint);
     Q_INVOKABLE void setRGPInIntraSagittal(const QVariantList& sagittalPoint);
     //粗配 投影正ce位片选点
     Q_INVOKABLE void setRGPInProjectedCoronal(const QVariantList& coronalPoint);
     Q_INVOKABLE void setRGPInProjectedSagittal(const QVariantList& sagittalPoint);
     //获取粗配的结果->3D椎骨的平移距离
     Q_INVOKABLE QVariantList get3DModelTranslationOfGeneralReg();

    //追踪钉的位姿矩阵,为单位矩阵
    Q_INVOKABLE QVariantList getNailMatrix();

    // QVariantList存储的3D点与矩阵的转换
    Q_INVOKABLE QVariantList point3DListToMatrixList(const QVariantList& point3DList);
    Q_INVOKABLE QVariantList matrixListToPoint3DList(const QVariantList& matrix3DList);

    Q_INVOKABLE void resetCalculator();

private:

    QVariantList point3DToQList(const Point3DType& point);
    Point3DType QList3DPointToPoint3D(const QVariantList& point3D);
    QVariantList Matrix4x4TypeToQList(const Matrix4x4Type* matrix);

    void PrintVtkMatrix(vtkMatrix4x4* mat);

    static Matrix4x4Type* generateInitCoronalMatrix();
    static Matrix4x4Type* generateInitSagittalMatrix();

    std::vector<cv::Vec3f> opencvHoughCirclesDetector(
        const DcmImage2DType* inputImage, const double minDist,
        const int minRadius = 0, const int maxRadius = 0,
        const double param1 = 100, const double param2 = 100,
        const std::string& showingName = "defaut");

    //转为调整像素间距后的3D点坐标
    Point3DType transform2DPointTo3DInSagittal(const Point3DType::pointer& point);
    Point3DType transform2DPointTo3DInCoronal(const Point3DType::pointer& point);

    Point3DType reSpacing3DPoint(const Point3DType& point,
        const double reSpacingScalar);

    double generateNailFeaturePoints();

private:

    double m_nailBallPhysicalRadius{ -1. };
    double m_nailCoronalRadius{ -1. };
    double m_nailSagittalRadius{ -1. };

    //最终使用的圆心 (如果交互传入圆心，则使用输入的值)
    Point3DType m_circleCenterInCoronal{0.}; // 没有重新设置像素间距使用的圆心
    Point3DType m_circleCenterInSagittal{0.};
    //chuanrude追踪钉尖端(suofangqian)
    Point3DType m_peakPointInCoronal{0.};
    Point3DType m_peakPointInSagittal{0.};

    //重新设置像素间距后的圆心位置（在正侧位片上）
    Point3DType m_reSpacingCenterInCoronal{0.};
    Point3DType m_reSpacingCenterInSagittal{0.};

    //重新设置像素间距后的尖端位置（在正侧位片上）
    Point3DType m_reSpacingPeakpointInCoronal{0.};
    Point3DType m_reSpacingPeakpointInSagittal{0.};

    //重新设置像素间距后 追踪钉 圆心和尖端（3D空间）  
    Point3DType m_reSpacingCenterPointIn3D{0.};
    Point3DType m_reSpacingPeakPointIn3D{0.};

    Matrix4x4Type* m_updatedCoronalMatrix;
    Matrix4x4Type* m_updatedSagittalMatrix;

    DcmImage2DType::SpacingType m_initSagittalSpacing{ 1. }; //chushituxiang 
    DcmImage2DType::SpacingType m_initCoronalSpacing{ 1. };

    DcmImage2DType::PointType m_initSagittalOrigin{0.};
    DcmImage2DType::PointType m_initCoronalOrigin{0.};

    double m_sagittalSpacing{ 1. }; //计算得到的像素间距
    double m_coronalSpacing{ 1. };

    double m_respacingScalarFactoryCoronal{ 1. };
    double m_respacingScalarFactorySagittal{ 1. };
    double m_intra2DImageDiffAlongZ{ 0. };

    //用于粗配的四个点
    Point3DType m_RGPInIntraCoronal{0.};
    Point3DType m_RGPInIntraSagittal{0.};
    Point3DType m_RGPInProjectedCoronal{0.};
    Point3DType m_RGPInProjectedSagittal{0.};
};
