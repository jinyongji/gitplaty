#ifndef GETJSON_H
#define GETJSON_H
#include <QObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QFile>
#include <QVariant>
class getJSON : public QObject{

    Q_OBJECT
public:
    getJSON(){}
    ~getJSON(){}
public:
    Q_INVOKABLE QVariantMap setPath(const QString& path)
    {
        QFile file(path);
        if(!file.open(QIODevice::ReadOnly))
        {
            emit errorInfo("File opend failed");
            return QVariantMap();
        }
        QJsonDocument jdc(QJsonDocument::fromJson(file.readAll()));
        QJsonObject obj = jdc.object();
        return obj.toVariantMap();
    }
signals:
    void errorInfo(const QString&);
};

#endif // GETJSON_H

