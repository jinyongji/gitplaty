﻿#pragma once

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageSeriesReader.h"
#include "itkGDCMImageIO.h"
#include "itkGDCMSeriesFileNames.h"
#include "itkImageToVTKImageFilter.h"
#include "itkResampleImageFilter.h"
#include "itkSimilarity2DTransform.h"
#include "itkSimilarity3DTransform.h"
#include "itkConstantPadImageFilter.h"
#include "itkChangeInformationImageFilter.h"
#include "itkCropImageFilter.h"

#include "vtkImageData.h"
#include "vtkMatrix4x4.h"



#include "opencv2/opencv.hpp"

#include <QObject>
#include <QList>
#include <QVariant>
#include <QDebug>
#include <QPointF>
#include <QMap>


using PixelType = signed short;
using DcmImage2DType = itk::Image< PixelType, 2U >;
using DcmImage3DType = itk::Image< PixelType, 3U >;

using VtkImage2D = vtkImageData;
using VtkImage3D = vtkImageData;

using RGSTPoints = itk::Point<double, 3U>;
using Similarity2DTransform = itk::Similarity2DTransform<double>;
using Similarity3Dtransform = itk::Similarity3DTransform<double>;
using ResampleImageFilter3D = itk::ResampleImageFilter<DcmImage3DType,DcmImage3DType>;
using ResampleImageFilter2D = itk::ResampleImageFilter<DcmImage2DType, DcmImage2DType>;
using IVConventerType3D = itk::ImageToVTKImageFilter<DcmImage3DType>;
using IVConventerType2D = itk::ImageToVTKImageFilter<DcmImage2DType>;
using ConstantPadImageFilter3D = itk::ConstantPadImageFilter<DcmImage3DType, DcmImage3DType>;
using ConstantPadImageFilter2D = itk::ConstantPadImageFilter<DcmImage2DType, DcmImage2DType>;
using ChangeInformationImageFilterType = itk::ChangeInformationImageFilter<DcmImage3DType>;
using CropImageFilter2D = itk::CropImageFilter<DcmImage2DType, DcmImage2DType>;

// ITK Image Data 的深拷贝
template<typename TPixel, unsigned int Dim>
void deepCopyItkImageData(const itk::Image<TPixel, Dim>* src, itk::Image<TPixel, Dim>* dst)
{
    if (!src || !dst) {
        return;
    }
    dst->CopyInformation(src);
    dst->SetMetaDataDictionary(src->GetMetaDataDictionary());
    dst->SetRegions(src->GetLargestPossibleRegion());
    dst->Allocate();
    std::copy_n(src->GetBufferPointer(),
        src->GetPixelContainer()->Size(), dst->GetBufferPointer());
}

template<typename TPixel, unsigned int Dim>
typename itk::Image<TPixel, Dim>::Pointer deepCopyItkImageData(const itk::Image<TPixel, Dim>* data)
{
    using ImageT = itk::Image<TPixel, Dim>;
    typename ImageT::Pointer ret = ImageT::New();
    deepCopyItkImageData(data, ret.GetPointer());
    return ret;
}

template<class T>
void make_shared(std::shared_ptr<T>& arg)
{
    arg = std::make_shared<T>();
}


//bug:
//没有限制 打点选择的次数
//没有限制 打点对应的actor
