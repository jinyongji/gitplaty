﻿#include "../include/nailBallRecorgnize.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkCastImageFilter.h"
#include "itkOpenCVImageBridge.h"

PoseCalculator::PoseCalculator()
    : m_updatedCoronalMatrix(Matrix4x4Type::New())
    , m_updatedSagittalMatrix(Matrix4x4Type::New())
{
}

PoseCalculator::~PoseCalculator()
{
}

void PoseCalculator::setNailBallPhysicalRadius(const qreal radiusInPhysical)
{
    if(radiusInPhysical<=0)
    {
        std::cout<<"error!! : "<<std::endl<<" nail ball phsical radius should greater than 0"<<std::endl;
        return;
    }
    m_nailBallPhysicalRadius = radiusInPhysical;
}

void PoseCalculator::setInitCoronalOrigin(const qreal origin0, const qreal origin1)
{
    m_initCoronalOrigin[0] = origin0;
    m_initCoronalOrigin[1] = origin1;
}

void PoseCalculator::setInitSagittalOrigin(const qreal origin0, const qreal origin1)
{
    m_initSagittalOrigin[0] = origin0;
    m_initSagittalOrigin[1] = origin1;
}

QVariantList PoseCalculator::Matrix4x4TypeToQList(
        const PoseCalculator::Matrix4x4Type *matrix)
{
    QVariantList list;
    for(int i =0;i<4;++i)
        for(int j=0;j<4;++j)
        {
            list.append(matrix->GetElement(i,j));
        }
    return list;
}

PoseCalculator::Matrix4x4Type* PoseCalculator::generateInitCoronalMatrix()
{
    PoseCalculator::Matrix4x4Type* initCoronalMatrix
        = PoseCalculator::Matrix4x4Type::New();
    initCoronalMatrix->Identity();
    /*   正位片绕x转-90
        1 0 0 0
        0 0 1 0
        0 -1 0 0
        0 0 0 1
    */
    initCoronalMatrix->SetElement(1, 1, 0);
    initCoronalMatrix->SetElement(1, 2, 1);
    initCoronalMatrix->SetElement(2, 1, -1);
    initCoronalMatrix->SetElement(2, 2, 0);

    return initCoronalMatrix;
}

PoseCalculator::Matrix4x4Type* PoseCalculator::generateInitSagittalMatrix()
{
    PoseCalculator::Matrix4x4Type* initSagittalMatrix
        = PoseCalculator::Matrix4x4Type::New();
    initSagittalMatrix->Identity();
    /*  侧位片绕y 转 -90，绕x -90,绕z 180
        0 0 1 0
        -1 0 0 0
        0 -1 0 0
        0 0 0 1
    */
    initSagittalMatrix->SetElement(0, 0, 0);
    initSagittalMatrix->SetElement(0, 2, 1);
    initSagittalMatrix->SetElement(1, 0, -1);
    initSagittalMatrix->SetElement(1, 1, 0);
    initSagittalMatrix->SetElement(2, 1, -1);
    initSagittalMatrix->SetElement(2, 2, 0);

    return initSagittalMatrix;
}

void PoseCalculator::setInitCoronalSpacing(
    const qreal spacing0,const qreal spacing1)
{
    m_initCoronalSpacing[0] =spacing0;
    m_initCoronalSpacing[1] =spacing1;
}

void PoseCalculator::setInitSagittalSpacing(
    const qreal spacing0,const qreal spacing1)
{
    m_initSagittalSpacing[0] =spacing0;
    m_initSagittalSpacing[1] =spacing1;
}

QVariantList PoseCalculator::getInitCoronalMatrix()
{
    return Matrix4x4TypeToQList(generateInitCoronalMatrix());
}

QVariantList PoseCalculator::getInitSagittalMatrix()
{
    return Matrix4x4TypeToQList(generateInitSagittalMatrix());
}

QVariantList PoseCalculator::getInitProjectedCoronalMatrix()
{
    QVariantList list;//rotate along X by 90 degree
    list.append(1);
    list.append(0);
    list.append(0);
    list.append(0);

    list.append(0);
    list.append(0);
    list.append(-1);
    list.append(0);

    list.append(0);
    list.append(1);
    list.append(0);
    list.append(0);

    list.append(0);
    list.append(0);
    list.append(0);
    list.append(1);

    return  list;
}

QVariantList PoseCalculator::getInitProjectedSagittalMatrix()
{
    QVariantList list;//rotate along y by -90 degree
    list.append(0);
    list.append(0);
    list.append(-1);
    list.append(0);

    list.append(0);
    list.append(1);
    list.append(0);
    list.append(0);

    list.append(1);
    list.append(0);
    list.append(0);
    list.append(0);

    list.append(0);
    list.append(0);
    list.append(0);
    list.append(1);

    return  list;
}

void PoseCalculator::setCoronalPeakPoints(const QVariantList &peakPointInCoronal)
{
    m_peakPointInCoronal = QList3DPointToPoint3D(peakPointInCoronal);
}

void PoseCalculator::setSagittalPeakPoints(const QVariantList &peakPointInSagittal)
{
    m_peakPointInSagittal = QList3DPointToPoint3D(peakPointInSagittal);
}

double PoseCalculator::getNewSpacingOfCoronal()
{
    return m_coronalSpacing;
}

double PoseCalculator::getNewSpacingOfSagittal()
{
    return m_sagittalSpacing;
}

QVariantList PoseCalculator::getRespacingCircleCenterInCoronal()
{
    return point3DToQList(m_reSpacingCenterInCoronal);
}

QVariantList PoseCalculator::getRespacingCircleCenterInSagittal()
{
    return point3DToQList(m_reSpacingCenterInSagittal);
}

QVariantList PoseCalculator::getRespacingPeakPointInCoronal()
{
    return point3DToQList(m_reSpacingPeakpointInCoronal);
}

QVariantList PoseCalculator::getRespacingPeakPointInSagittal()
{
    return point3DToQList(m_reSpacingPeakpointInSagittal);
}

 void PoseCalculator::setCoronalBallRadius(const qreal nailCoronalRadius)
 {
    m_nailCoronalRadius =nailCoronalRadius;
 }

void PoseCalculator::setSagittalBallRadius(const qreal nailsagittalRadius)
{
    m_nailSagittalRadius = nailsagittalRadius;
}

void PoseCalculator::setCoronalBallCenter( 
    const qreal coronalCircleCenterX,const qreal coronalCircleCenterY)
{
    Point2DType centerInCoronal2D{ 0 };
    centerInCoronal2D[0] = coronalCircleCenterX;//圆心像素坐标x
    centerInCoronal2D[1] = coronalCircleCenterY;//圆心像素坐标y
    m_circleCenterInCoronal = transform2DPointTo3DInCoronal(centerInCoronal2D.GetDataPointer());
}

void PoseCalculator::setSagittalBallCenter(
           const qreal sagittalCircleCenterX,const qreal sagittalCircleCenterY)
{   
    Point2DType centerInSagittal2D{ 0 };
    centerInSagittal2D[0] = sagittalCircleCenterX;//圆心像素坐标x
    centerInSagittal2D[1] = sagittalCircleCenterY;//圆心像素坐标y
    m_circleCenterInSagittal   = transform2DPointTo3DInSagittal(centerInSagittal2D.GetDataPointer());
}

void PoseCalculator::PrintVtkMatrix(vtkMatrix4x4* mat)

{
    std::cout << std::endl << "Matrix: " << std::endl;
    for (int i = 0; i < 4; ++i)
    {
        std::cout << "\t";
        for (int j = 0; j < 4; ++j)
        {
            std::cout << std::setprecision(16) << mat->GetElement(i, j) << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl<< std::endl;
}

void PoseCalculator::calculateUseInteractData()
{
    //像素间距计算
    m_coronalSpacing = m_nailBallPhysicalRadius / m_nailCoronalRadius;
    m_sagittalSpacing = m_nailBallPhysicalRadius / m_nailSagittalRadius;
    std::cout << " caculated intra coronal spacing=  " << m_coronalSpacing << std::endl;
    std::cout << " caculated intra sagittal spacing=  " << m_sagittalSpacing<< std::endl;

    //术中2D矩阵更新
    m_updatedSagittalMatrix= generateInitSagittalMatrix();//侧位片为基准不动
    m_updatedCoronalMatrix = generateInitCoronalMatrix();
    m_intra2DImageDiffAlongZ = generateNailFeaturePoints();//计算追踪钉3D特征点
    m_updatedCoronalMatrix->SetElement(2, 3,
        m_updatedCoronalMatrix->GetElement(2, 3) + m_intra2DImageDiffAlongZ);
}

Q_INVOKABLE QVariantList PoseCalculator::getUpdatedCoronalMatrix()
{
    return Matrix4x4TypeToQList(m_updatedCoronalMatrix);
}

Q_INVOKABLE QVariantList PoseCalculator::getUpdatedSagittalMatrix()
{
    return Matrix4x4TypeToQList(m_updatedSagittalMatrix);
}

double PoseCalculator::getSaclarFactorSagittal()
{
    return m_respacingScalarFactorySagittal;
}

double PoseCalculator::getSaclarFactorCoronal()
{
    return m_respacingScalarFactoryCoronal;
}

double PoseCalculator::getIntra2DImageDiffAlongZ()
{
    return m_intra2DImageDiffAlongZ;
}

QVariantList PoseCalculator::getRespacingNailCenterIn3D ()
{
    return point3DToQList( m_reSpacingCenterPointIn3D);
}

QVariantList PoseCalculator::getRespacingNailPeakIn3D()
{
    return point3DToQList(m_reSpacingPeakPointIn3D);
}

void PoseCalculator::setRGPInIntraCoronal(const QVariantList &coronalPoint)
{
    m_RGPInIntraCoronal = QList3DPointToPoint3D(coronalPoint);
}

void PoseCalculator::setRGPInIntraSagittal(const QVariantList &sagittalPoint)
{
    m_RGPInIntraSagittal= QList3DPointToPoint3D(sagittalPoint);
}

void PoseCalculator::setRGPInProjectedCoronal(const QVariantList &coronalPoint)
{
    m_RGPInProjectedCoronal =QList3DPointToPoint3D(coronalPoint);
}

void PoseCalculator::setRGPInProjectedSagittal(const QVariantList &sagittalPoint)
{
    m_RGPInProjectedSagittal=QList3DPointToPoint3D(sagittalPoint);
}

QVariantList PoseCalculator::get3DModelTranslationOfGeneralReg()
{
    QVariantList translationOrgin;
    translationOrgin.append(m_RGPInIntraCoronal[0] - m_RGPInProjectedCoronal[0]);
    translationOrgin.append(m_RGPInIntraSagittal[1] - m_RGPInProjectedSagittal[1]);
    translationOrgin.append(m_RGPInIntraSagittal[2] - m_RGPInProjectedSagittal[2]);
    std::cout<<"translationOrgin: "<<m_RGPInIntraCoronal[0] - m_RGPInProjectedCoronal[0]
            <<" "<<m_RGPInIntraSagittal[1] - m_RGPInProjectedSagittal[1]
            <<" "<<m_RGPInIntraSagittal[2] - m_RGPInProjectedSagittal[2]<<std::endl;
    return  translationOrgin;
}

QVariantList PoseCalculator::getNailMatrix()
{
    return Matrix4x4TypeToQList(Matrix4x4Type::New());
}

QVariantList PoseCalculator::point3DListToMatrixList(const QVariantList &point3DList)
{
    QVariantList matrixList;
    if(point3DList.size()!=3)
    {
        std::cout<<" point3DList should has 3 element"<<std::endl;
        return matrixList;
    }

    matrixList.append(1);
    matrixList.append(0);
    matrixList.append(0);
    matrixList.append(point3DList[0]);//x

    matrixList.append(0);
    matrixList.append(1);
    matrixList.append(0);
    matrixList.append(point3DList[1]);//y

    matrixList.append(0);
    matrixList.append(0);
    matrixList.append(1);
    matrixList.append(point3DList[2]);//z

    matrixList.append(0);
    matrixList.append(0);
    matrixList.append(0);
    matrixList.append(1);

    return matrixList;
}

QVariantList PoseCalculator::matrixListToPoint3DList(const QVariantList &matrix3DList)
{

    QVariantList point3DList;
    if(matrix3DList.size()!=16)
    {
        std::cout<<" matrix3DList should has 16 element"<<std::endl;
        return  point3DList;
    }

    point3DList[0] = matrix3DList[3];
    point3DList[1] = matrix3DList[7];
    point3DList[2] = matrix3DList[11];

    return  point3DList;
}

void PoseCalculator::resetCalculator()
{
    m_nailBallPhysicalRadius = -1;
    m_nailCoronalRadius= -1;
    m_nailSagittalRadius= -1;

    m_coronalSpacing  = 1.0;
    m_sagittalSpacing = 1.0;

    m_reSpacingCenterInCoronal.Fill(0.0);
    m_reSpacingCenterInSagittal.Fill(0.0);

    m_reSpacingPeakpointInCoronal.Fill(0.0);
    m_reSpacingPeakpointInSagittal.Fill(0.0);

    m_reSpacingCenterPointIn3D.Fill(0.0);
    m_reSpacingPeakPointIn3D.Fill(0.0);

    m_updatedCoronalMatrix->Identity();
    m_updatedSagittalMatrix->Identity();

    m_initSagittalSpacing = 1.0;
    m_initCoronalSpacing = 1.0;

    m_initSagittalOrigin.Fill(0.0);
    m_initCoronalOrigin.Fill(0.0);

    m_sagittalSpacing = 1.0;
    m_coronalSpacing = 1.0;

    m_respacingScalarFactoryCoronal = 1.0;
    m_respacingScalarFactorySagittal = 1.0;
    m_intra2DImageDiffAlongZ = 0.0;

    m_RGPInIntraCoronal.Fill(0.0);
    m_RGPInIntraSagittal.Fill(0.0);
    m_RGPInProjectedCoronal.Fill(0.0);
    m_RGPInProjectedSagittal.Fill(0.0);
}

QVariantList PoseCalculator::point3DToQList(const PoseCalculator::Point3DType &point)
{
    QVariantList list;
    list.append(1);
    list.append(0);
    list.append(0);
    list.append(point[0]);//x

    list.append(0);
    list.append(1);
    list.append(0);
    list.append(point[1]);//y

    list.append(0);
    list.append(0);
    list.append(1);
    list.append(point[2]);//z

    list.append(0);
    list.append(0);
    list.append(0);
    list.append(1);

    return list;
}

PoseCalculator::Point3DType PoseCalculator::QList3DPointToPoint3D(const QVariantList &point3D)
{
    Point3DType tempPoint{0};
    if(point3D.size()!=3)
    {
        return  tempPoint;
    }
    for(int i=0;i<3;++i)
    {
        tempPoint.SetElement(i,point3D[i].toDouble());
    }
    return  tempPoint;
}

PoseCalculator::Point3DType 
PoseCalculator::transform2DPointTo3DInSagittal(
    const Point2DType::pointer& point)
{
    Point3DType point3D{ 0 };
    point3D[0] = 0;//侧位片上，x=0
    point3D[1] = m_initSagittalOrigin[0] - point[0] * m_initSagittalSpacing[0]; //2D x -> 3D y
    point3D[2] = m_initSagittalOrigin[1] - point[1] * m_initSagittalSpacing[1]; //2D y -> 3D z
    return point3D;
}

PoseCalculator::Point3DType
PoseCalculator::transform2DPointTo3DInCoronal(
    const Point3DType::pointer& point)
{
    Point3DType point3D{ 0 };
    point3D[0] = m_initCoronalOrigin[0] + point[0] * m_initCoronalSpacing[0]; //2D x -> 3D x
    point3D[1] = 0;//正位片上，y=0
    point3D[2] = m_initCoronalOrigin[1] - point[1] * m_initCoronalSpacing[1]; //2D y -> 3D z
    return point3D;
}

//三个方向都缩放同一因子
PoseCalculator::Point3DType PoseCalculator::reSpacing3DPoint(
    const Point3DType& point,const double reSpacingScalar)
{
    Point3DType newPoint{ 0 };
    newPoint[0] = point[0] * reSpacingScalar;
    newPoint[1] = point[1] * reSpacingScalar;
    newPoint[2] = point[2] * reSpacingScalar;
    return newPoint;
}

double PoseCalculator::generateNailFeaturePoints()
{
    //追踪钉 3D位置计算 要采用新的像素间距进行计算
    //假设术中2D图像的xy像素间距一致！！
    m_respacingScalarFactoryCoronal = m_coronalSpacing / m_initCoronalSpacing[0];
    m_respacingScalarFactorySagittal = m_sagittalSpacing/ m_initSagittalSpacing[0];
    //本处为正侧位片上的3D点，其中一个方向始终为0，其他两个方向缩放程度相等，故可调用此函数
    m_reSpacingCenterInCoronal = reSpacing3DPoint(m_circleCenterInCoronal, m_respacingScalarFactoryCoronal);
    m_reSpacingCenterInSagittal = reSpacing3DPoint(m_circleCenterInSagittal, m_respacingScalarFactorySagittal);

    m_reSpacingCenterPointIn3D[0] = m_reSpacingCenterInCoronal[0];
    m_reSpacingCenterPointIn3D[1] = m_reSpacingCenterInSagittal[1];
    m_reSpacingCenterPointIn3D[2] = m_reSpacingCenterInSagittal[2]; //以侧位片为基准

    m_reSpacingPeakpointInCoronal = reSpacing3DPoint(m_peakPointInCoronal, m_respacingScalarFactoryCoronal);
    m_reSpacingPeakpointInSagittal = reSpacing3DPoint(m_peakPointInSagittal, m_respacingScalarFactorySagittal);
    m_reSpacingPeakPointIn3D[0] = m_reSpacingPeakpointInCoronal[0];
    m_reSpacingPeakPointIn3D[1] = m_reSpacingPeakpointInSagittal[1];
    m_reSpacingPeakPointIn3D[2] = m_reSpacingPeakpointInSagittal[2]; //以侧位片为基准

    //计算正侧位片z方向的偏差
    double deviationAlongZCenter = m_reSpacingCenterInSagittal[2] - m_reSpacingCenterInCoronal[2];
    std::cout<<"deviationAlongZCenter = "<<deviationAlongZCenter<<std::endl;
    double deviationAlongZPeak = m_reSpacingPeakpointInSagittal[2] - m_reSpacingPeakpointInCoronal[2];
    std::cout<<"deviationAlongZPeak = "<<deviationAlongZPeak<<std::endl;
    if (std::abs(deviationAlongZCenter - deviationAlongZPeak) > 1)
    {
        std::cout << "warning!! : deviation along z of center point is: " << deviationAlongZCenter
            << std::endl<<"deviation along z of peak point is: " << deviationAlongZPeak
            << "!! the difference is bigger than 1mm" << std::endl;
        return deviationAlongZCenter;

    }
    else
    {
        return (deviationAlongZCenter + deviationAlongZPeak) / 2.0;
    }
    
}

