#ifndef NavViewGlobal_h
#define NavViewGlobal_h
#include <QString>
#include <QMap>
#include <QVariant>
#include "itkImage.h"

namespace fiui
{
    enum EventType
    {
        addActor = 0,
        removeActor,
        modifyActor,
        removeAllActor,
        modifyActorData,
        addStage = 10,
        modifyStage,
        removeStage,
        resetStageCamera,
        setLayoutValue,
        addStageToLayout,
        removeStageFromLayout,
        addActorToStage = 20,
        removeActorFromStage,
        initialise = 40
    };

    const QVariantList viewHandleEvent = {
        EventType::removeActor,EventType::removeAllActor,EventType::removeStage,
        EventType::addActorToStage,EventType::removeActorFromStage,EventType::initialise};

    const QVariantList subViewHandleEvent = {
        EventType::addStage,EventType::modifyStage,EventType::resetStageCamera,
        EventType::setLayoutValue,EventType::addStageToLayout,EventType::removeStageFromLayout};

    const QVariantList actorHandleEvent = {
        EventType::addActor,EventType::modifyActor,EventType::modifyActorData};
    
    enum LayoutType
    {
        FourWindow = 0,
        ThreeWindow = 1,
        UserDefine = 10
    };

    struct NavViewEvent
    {
        EventType       eventType;
        QString         string1;
        QString         string2;
        QString         string3;
        quint32         intParam;
        QVariantMap     eventMap;
        QVariantList    eventList;
    };

    using DcmPixelType = signed short;
    const unsigned int Dcm2DDimention = 2;
    const unsigned int Dcm3DDimention = 3;

    using DcmImage2DType = itk::Image<DcmPixelType , Dcm2DDimention>;
    using DcmImage3DType = itk::Image<DcmPixelType , Dcm3DDimention>;

    enum class AxisType
    {
        AXIS_X,
        AXIS_Y,
        AXIS_Z
    };

    enum AnatomyPlane {
        SagittalPlane = 0,              ///< 矢状面         left right cutting
        CoronalPlane,                   ///< 冠状面         front back cutting
        TransversePlane,                ///< 横断面(轴位面)  up down cutting 
    };

    enum class projectionMethod
    {
        MAXIMUM = 0 ,
        MEAN,
        MEDIAN,
        MINIMUM,
        STANDARDDEVIATION,
        SUM
    };

    enum ImageOriginPosition {
            Unchanged = 0,
            ImageCenter,
            AxisOrigin
    };
    
    const QVariantList allTypeList = {
        "dicom3d","project3d","slice3d","dicom2d",
        "tool","sphere","cylinder","axis"};
    
    const QVariantList dataTypeList = {
        "dicom3d","project3d","slice3d","dicom2d",
        "tool"};

    // const QVariantList userTypeList = {"image","tools","accessory","widget"};

    const QVariantList viewTypeList = {"3d","2d"};
    const QVariantList directionList = {"sagittal","coronal","axial"};
}
#endif // !NavViewGlobal_h