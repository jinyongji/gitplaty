#ifndef NavCylinderActor_h
#define NavCylinderActor_h

#include "NavShapeActor.h"
#include <QVariantList>
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>

class vtkCylinderSource;
namespace fiui
{
    class NavCylinderActor :public NavShapeActor
    {
    public:
        NavCylinderActor();
        ~NavCylinderActor();

        bool InitParams(const QVariantMap& data);
        bool SetParams(const QVariantMap& newData);
        void GetParams(QVariantMap& rawData);

        void SetPoints(QVariantList& point1,QVariantList& point2);
        double GetDist(QVariantList& point1,QVariantList& point2);
        void SetRadius(double);
        double GetRadius();
        void SetHeight(double);
        double GetHeight();
        void SetResolution(int);
        int GetResolution();
    private:
        vtkCylinderSource*              m_cylinderSrc;
        vtkTransform*                   m_transform;
        vtkTransformPolyDataFilter*     m_transFilter;
        bool                            m_isByPoint;
        QVariantList                    m_point1;
        QVariantList                    m_point2;
    };
}

#endif // !NavCylinderActor_h