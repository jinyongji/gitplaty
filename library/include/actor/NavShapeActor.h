#ifndef NavShapeActor_h
#define NavShapeActor_h

#include "NavActor.h"

class vtkAbstractMapper3D;
namespace fiui
{
    class NavShapeActor :public NavActor
    {
    public:
        NavShapeActor();
        virtual ~NavShapeActor();

        virtual bool InitParams(const QVariantMap& data);
        virtual bool SetParams(const QVariantMap& newData);
        virtual void GetParams(QVariantMap& rawData);
        
        void SetColor(QVariantList&);
        QVariantList GetColor();
    protected:
        vtkAbstractMapper3D* m_mapper;
    };
}

#endif // !NavShapeActor_h