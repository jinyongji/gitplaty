#ifndef NavDicomProjectActor_h
#define NavDicomProjectActor_h

#include "NavDicom3dActor.h"
#include <vtkLookupTable.h>
#include <vtkTransform.h>
#include "itkMatrixOffsetTransformBase.h"

namespace fiui
{
    class NavDicomVolumeActor;
    class NavDicomProjectActor : public NavDicom3dActor
    {
        friend class NavDicomVolumeActor;
        using ConstantPadImageFilter3D =
            itk::ConstantPadImageFilter<DcmImage3DType, DcmImage3DType>;

        using Similarity3Dtransform = itk::Similarity3DTransform<>;
        using ResampleImageFilter3D =
            itk::ResampleImageFilter<DcmImage3DType, DcmImage3DType>;

        using MaximumProjectionImageFilter =
            itk::MaximumProjectionImageFilter<DcmImage3DType, DcmImage2DType>;
        using MeanProjectionImageFilter =
            itk::MeanProjectionImageFilter<DcmImage3DType, DcmImage2DType>;
        using MedianProjectionImageFilter =
            itk::MedianProjectionImageFilter<DcmImage3DType, DcmImage2DType>;
        using MinimumProjectionImageFilter =
            itk::MinimumProjectionImageFilter<DcmImage3DType, DcmImage2DType>;
        using StandardDeviationProjectionImageFilter =
            itk::StandardDeviationProjectionImageFilter<DcmImage3DType, DcmImage2DType>;
        using SumProjectionImageFilte =
            itk::SumProjectionImageFilter<DcmImage3DType, DcmImage2DType>;
        using IVConventerFilter2D = itk::ImageToVTKImageFilter<DcmImage2DType>;
    public:
        NavDicomProjectActor();
        ~NavDicomProjectActor();
        NavDicomProjectActor(const NavDicomProjectActor&) = delete;
        void operator=(const NavDicomProjectActor&) = delete;
        
        bool InitParams(const QVariantMap& data);
        bool SetParams(const QVariantMap& newData);
        void GetParams(QVariantMap& rawData);
        bool SetDataAciton(const QVariantMap& rawData,QVariantMap& result);
        void SetInnerOrigin(QVariantList& origin);

        void PipeLineInit();
        void PadImageUseConstant(
            const DcmImage3DType::SizeType& lowerExtendRegion,
            const DcmImage3DType::SizeType& upperExtendRegion,
            const DcmImage3DType::PixelType& constant);

        void RotateImage(const AxisType& whichAxis, const double angleInDegree,
            const DcmImage3DType::PointType& originTranslation ={ 0 });
        
        void SetImageSpacing(QVariantList& spacing);
        void ImageOriginTranslation(const double x, const double y, const double z);

        QVariantList GetInnerOrigin();
        QVariantList GetImageSpacing();
        QVariantList GetImageSize();
        QVariantList GetImageIndex();
        std::array<double, 3> getRotatedAngleInDegree();

        void SetColorWindow(double);
        double GetColorWindow();
        void SetColorLevel(double);
        double GetColorLevel();
        void SetOpacityEffect(bool);
        bool GetOpacityEffect();

        void ResetTransform();

        void UpdateVtkData();
        void UpdateVtkDataDirect();
    private:
        std::array<double, 3>                       m_rotatedAngle;
        AnatomyPlane                                m_plane;
        projectionMethod                            m_projecntionMethod;
        Similarity3Dtransform::Pointer              m_transform;
        ResampleImageFilter3D::Pointer              m_resampleFilter;
        ConstantPadImageFilter3D::Pointer           m_padFilter;
        MaximumProjectionImageFilter::Pointer               m_maxFilter;
        MeanProjectionImageFilter::Pointer                  m_meanFilter;
        MedianProjectionImageFilter::Pointer                m_medianFilter;
        MinimumProjectionImageFilter::Pointer               m_minFilter;
        SumProjectionImageFilte::Pointer                    m_sumFilter;
        StandardDeviationProjectionImageFilter::Pointer     m_SDFilter;
        IVConventerFilter2D::Pointer                        m_IVConventer2D;
        vtkLookupTable*                                     m_lookTable;

        DcmImage3DType::SizeType                            m_lowerExtendRegion;
        DcmImage3DType::SizeType                            m_upperExtendRegion;
        DcmImage3DType::PixelType                           m_constant;
        Similarity3Dtransform::InputPointType               m_rotationCenter;
        QVector3D                                           m_spacing;
        bool                                                m_opacityEffect;
        QString                                             m_parentId;
    };
    
}


#endif // !NavDicomProjectActor_h