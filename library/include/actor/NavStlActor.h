#ifndef NavStlActor_h
#define NavStlActor_h

#include "NavShapeActor.h"
#include <QVariantList>

class vtkSTLReader;
namespace fiui
{
    class NavStlActor :public NavShapeActor
    {
    public:
        NavStlActor();
        ~NavStlActor();

        bool InitParams(const QVariantMap& data);
        bool SetParams(const QVariantMap& newData);
        void GetParams(QVariantMap& rawData);
    private:
        bool RequestRead();
        vtkSTLReader* m_reader;
        QString m_path;
    };
}

#endif // !NavStlActor_h