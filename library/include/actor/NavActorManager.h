#ifndef NavActorManager_h
#define NavActorManager_h
#include <QQuickFramebufferObject>
#include <QList>
#include "../NavViewGlobal.h"
#include "NavActor.h"

namespace fiui
{
    class NavView;
    class NavActorManager
    {
    public:
        NavActorManager(NavView* view);
        ~NavActorManager();
        NavActorManager(const NavActorManager&) = delete;
        void operator=(const NavActorManager&) = delete;

        void handleActorEvent(NavViewEvent* event);
        QVariantMap* getMapActorsInfo();

        bool addActor(const QString& type,const QString& userType,const QString& id,const QVariantMap& map);
        void removeActor(const QString& id);
        void modifyActor(const QString& id,const QVariantMap& map);
        void modifyActorData(const QString& id,const QVariantMap& map);
        void removeAllActor();
        void updateVtkData(const QList<QString>& actorlist);
        
        QVariantMap getActorMap();
        QVariantMap getActorParams(const QString& id);
        NavActor* getActor(const QString& id);
        bool findActor(quint64 prop,QString& id);
    protected:
        bool InnerRemoveAllItem();
        void AddShareDataActor(const QString& source,const QString& dist);
        void RemoveShareDataActor(const QString& id);
        void UpdateShareDataActor(const QString& id);
    private:
        QMap<QString,NavActor*>                         m_mapActors;
        QVariantMap                                     m_mapActorsInfo;
        NavView*                                        m_viewer;
        QList<QString>                                  m_shareDataGroup;
    };
}


#endif // !NavActorManager_h