#ifndef NavDicom3dActor_h
#define NavDicom3dActor_h

#include "NavActor.h"
#include <array>
#include "itkGDCMImageIO.h"
#include "itkImageSeriesReader.h"
#include "itkGDCMSeriesFileNames.h"
#include "itkChangeInformationImageFilter.h"
#include "itkImageToVTKImageFilter.h"
#include "itkConstantPadImageFilter.h"
#include "itkSimilarity3DTransform.h"
#include "itkResampleImageFilter.h"
#include "itkMaximumProjectionImageFilter.h"
#include "itkMeanProjectionImageFilter.h"
#include "itkMedianProjectionImageFilter.h"
#include "itkMinimumProjectionImageFilter.h"
#include "itkStandardDeviationProjectionImageFilter.h"
#include "itkSumProjectionImageFilter.h"
#include "../NavViewGlobal.h"

namespace fiui
{
    class NavDicom3dActor : public NavActor
    {
    public:
        NavDicom3dActor();
        virtual ~NavDicom3dActor();
        NavDicom3dActor(const NavDicom3dActor&) = delete;
        void operator=(const NavDicom3dActor&) = delete;

        virtual bool InitParams(const QVariantMap& data);
        virtual bool SetParams(const QVariantMap& newData);
        virtual void GetParams(QVariantMap& rawData);

        QString GetPath();
        quint32 GetOriginChange();
        bool IsReadSuccess();
        
        virtual void SetInnerOrigin(QVariantList& origin);
        virtual QVariantList GetInnerOrigin();

        virtual QVariantList GetImageSpacing();
        // void setImageSpacing(QVector3D& spacing);

        virtual QVariantList GetImageSize();
        virtual QVariantList GetImageIndex();

        virtual void UpdateVtkData();
        virtual void UpdateVtkDataDirect();
        using ImageIOType = itk::GDCMImageIO;
        using ImageReaderType = itk::ImageSeriesReader<DcmImage3DType>;
        using NamesGeneratorType = itk::GDCMSeriesFileNames;
        using ChangeInformationImageFilterType = itk::ChangeInformationImageFilter<DcmImage3DType>;
    protected:
        bool RequestRead();
        DcmImage3DType::Pointer                     m_ITKImageData;
        NamesGeneratorType::Pointer                 m_namesGenerator;
        ImageReaderType::FileNamesContainer         m_vecDicomFiles;
        ImageReaderType::Pointer                    m_seriesReader;
        ChangeInformationImageFilterType::Pointer   m_changeInfoFilter;
        QString                                     m_path;
        bool                                        m_isReadSuccess;
        QVector3D                                   m_origin;
        quint32                                     m_originChange;
        QString                                     m_dataSource;
        // QVector3D                                   m_spacing;
    };
    
}


#endif // !NavDicom3dActor_h