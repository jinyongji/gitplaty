#ifndef NavSphereActor_h
#define NavSphereActor_h

#include "NavShapeActor.h"

class vtkSphereSource;
namespace fiui
{
    class NavSphereActor :public NavShapeActor
    {
    public:
        NavSphereActor();
        ~NavSphereActor();

        bool InitParams(const QVariantMap& data);
        bool SetParams(const QVariantMap& newData);
        void GetParams(QVariantMap& rawData);

        void SetRadius(double);
        double GetRadius();
        void SetThetaResolution(int);
        int GetThetaResolution();
        void SetPhiResolution(int);
        int GetPhiResolution();
    private:
        vtkSphereSource*    m_sphereSrc;
    };
}

#endif // !NavSphereActor_h