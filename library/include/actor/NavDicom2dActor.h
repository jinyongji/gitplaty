#ifndef NavDicom2dActor_h
#define NavDicom2dActor_h

#include "NavActor.h"
#include "../NavViewGlobal.h"

#include <array>

#include "itkSimilarity2DTransform.h"
#include "itkResampleImageFilter.h"
#include "itkImageToVTKImageFilter.h"
#include <itkImageFileReader.h>
#include <itkGDCMImageIO.h>
#include "itkRescaleIntensityImageFilter.h"
#include "itkCastImageFilter.h"
#include "itkOpenCVImageBridge.h"

#include "vtkSmartPointer.h"

#include "opencv2/opencv.hpp"

class vtkLookupTable;
namespace fiui
{
    class NavDicom2dActor : public NavActor
    {
    public:
        NavDicom2dActor();
        ~NavDicom2dActor();
        NavDicom2dActor(const NavDicom2dActor&) = delete;
        void operator=(const NavDicom2dActor&) = delete;

        bool InitParams(const QVariantMap& data);
        bool SetParams(const QVariantMap& newData);
        void GetParams(QVariantMap& rawData);
        bool SetDataAciton(const QVariantMap& rawData,QVariantMap& result);
        void detectorAction(const QVariantMap& rawMap,QVariantMap& newMap);
        
        QString GetPath();
        
        void SetInnerOrigin(QVariantList& origin);
        QVariantList GetInnerOrigin();

        void SetImageSpacing(QVariantList& spacing);
        QVariantList GetImageSpacing();
        QVariantList GetImageSize();
        QVariantList GetImageIndex();
        
        std::vector<double>  opencvHoughCirclesDetector(
            const double minDist,const int minRadius = 0, const int maxRadius = 0,
            const double param1 = 100, const double param2 = 100);
        
        void resampleAITK2DImage(
            const double angleInDegree,const DcmImage2DType::SpacingType& newSpacing,
            const DcmImage2DType::SizeType& newSize,
            const DcmImage2DType::PointType& originTranslation ={ 0 });

        void SetColorWindow(double);
        double GetColorWindow();
        void SetColorLevel(double);
        double GetColorLevel();
        void SetOpacityEffect(bool);
        bool GetOpacityEffect();
        virtual void UpdateVtkData();
        virtual void UpdateVtkDataDirect();
    protected:
        using ItkDicomImageIO = itk::GDCMImageIO;
        using ReaderType = itk::ImageFileReader<DcmImage2DType>;
        using IVConventerType = itk::ImageToVTKImageFilter<DcmImage2DType>;
        using Similarity2DTransform = itk::Similarity2DTransform<double>;
        using ResampleFilterType = itk::ResampleImageFilter<DcmImage2DType, DcmImage2DType>;
        using RescalerType = itk::RescaleIntensityImageFilter<DcmImage2DType, DcmImage2DType>;
        using bmpPixelType = unsigned char;
        using BMPImageType = itk::Image<bmpPixelType, 2U>;
        using CastFilterType = itk::CastImageFilter<DcmImage2DType, BMPImageType>;
    private:
        bool RequestRead();
        void PipeLineInit();
        void ResetRotationCenter();
        
        ItkDicomImageIO::Pointer        m_dicomIO;
        ReaderType::Pointer             m_singleReader;
        DcmImage2DType::Pointer         m_ITKImageData;
        IVConventerType::Pointer        m_conventer;
        ResampleFilterType::Pointer     m_resamplFilter;
        RescalerType::Pointer           m_intensityRescaler; 
        CastFilterType::Pointer         m_caster;
        Similarity2DTransform::Pointer  m_transform;
        RescalerType::Pointer           m_rescalefilter;
        vtkLookupTable*                 m_lookTable;
        QString                         m_path;
        bool                            m_opacityEffect;
        double                          m_radians;
    };
    
}


#endif // !NavDicom2dActor_h