#ifndef NavDicomVolumeActor_h
#define NavDicomVolumeActor_h

#include "NavDicom3dActor.h"
#include <vtkTransform.h>

class vtkAbstractMapper3D;
namespace fiui
{
    class NavDicomProjectActor;
    class NavDicomVolumeActor : public NavDicom3dActor
    {
    public:
        NavDicomVolumeActor();
        ~NavDicomVolumeActor();
        NavDicomVolumeActor(const NavDicom3dActor&) = delete;
        void operator=(const NavDicom3dActor&) = delete;

        bool InitParams(const QVariantMap& data);
        bool SetParams(const QVariantMap& newData);
        void GetParams(QVariantMap& rawData);
        bool SetDataAciton(const QVariantMap& rawData,QVariantMap& result);

        void SetThreshold(double);
        double GetThreshold();
        NavActor* GetShareDataActor(const QString& type,const QVariantMap& data);
        void UpdateVtkData();
        void UpdateVtkDataDirect();
        using IVConventerFilter3D = itk::ImageToVTKImageFilter<DcmImage3DType>;
    private:
        void RotateImage(const AxisType& whichAxis, const double angleInDegree);

        IVConventerFilter3D::Pointer    m_imageToVtkDataFilter;
        vtkAbstractMapper3D*            m_mapper;
        double                          m_threshold;
        std::array<double, 3>           m_rotatedAngle;
        vtkTransform*                   m_rotateTransform;
    };
    
}


#endif // !NavDicomVolumeActor_h