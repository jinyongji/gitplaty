#ifndef NavAxisActor_h
#define NavAxisActor_h

#include "NavActor.h"

class vtkAxesActor;
namespace fiui
{
    class NavAxisActor :public NavActor
    {
    public:
        NavAxisActor();
        ~NavAxisActor();

        bool InitParams(const QVariantMap& data);
        bool SetParams(const QVariantMap& newData);
        void GetParams(QVariantMap& rawData);

        void SetLength(QVariantList&);
        QVariantList GetLength();
        void SetConeResolution(int);
        int GetConeResolution();
        void SetCylinderResolution(int);
        int GetCylinderResolution();
        void SetConeRadius(double);
        double GetConeRadius();
        void SetCylinderRadius(double);
        double GetCylinderRadius();
        void SetAxisLabel(QVariantList&);
        QVariantList GetAxisLabel();
        void UseLabel(bool);
        bool IsLabel();

    };
}

#endif // !NavAxisActor_h