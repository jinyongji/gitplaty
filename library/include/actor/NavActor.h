#ifndef NavActor_h
#define NavActor_h

#include <vtkProp3D.h>
#include "Position.h"
#include <QList>
#include <QVariantMap>
#include <QVector3D>

namespace fiui
{
    class NavStage;
    class NavActor
    {
        friend class NavStage;
    public:
        NavActor();
        virtual ~NavActor();
        NavActor(const NavActor&) = delete;
        void operator=(const NavActor&) = delete;
        
        virtual bool InitParams(const QVariantMap& data);
        virtual bool SetParams(const QVariantMap& newData);
        virtual void GetParams(QVariantMap& rawData);
        virtual bool SetDataAciton(const QVariantMap& rawData,QVariantMap& result);
        virtual NavActor* GetShareDataActor(const QString& type,const QVariantMap& data);

        void OnStage(NavStage* stage);
        void OffStage(NavStage* stage);

        void SetPosition(fib::Position& position);
        fib::Position GetPosition();
        virtual void SetInnerOrigin(QVariantList& origin);
        virtual QVariantList GetInnerOrigin();
        void SetPickable(bool);
        bool GetPickable();
        void SetVisibility(bool);
        bool GetVisibility();

        bool HasProp(vtkProp* prop);

        virtual void UpdateVtkDataDirect();
    protected:
        vtkProp3D*              m_prop3D;
    };
    
}


#endif // !NavActor_h