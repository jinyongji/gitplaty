#ifndef NavSlice3dActor_h
#define NavSlice3dActor_h

#include "NavDicom3dActor.h"
#include "Position.h"
class vtkAbstractMapper3D;
namespace fiui
{
    class NavSlice3dActor :public NavDicom3dActor
    {
        friend class NavDicomVolumeActor;
    public:
        NavSlice3dActor();
        ~NavSlice3dActor();

        bool InitParams(const QVariantMap& data);
        bool SetParams(const QVariantMap& newData);
        void GetParams(QVariantMap& rawData);

        void SetColorWindow(double);
        double GetColorWindow();
        void SetColorLevel(double);
        double GetColorLevel();
        void SetPlanePosition(fib::Position& position);
        fib::Position GetPlanePosition();
        void UpdateVtkData();
        void UpdateVtkDataDirect();
        
        using IVConventerFilter3D = itk::ImageToVTKImageFilter<DcmImage3DType>;
    private:
        IVConventerFilter3D::Pointer    m_imageToVtkDataFilter;
        vtkAbstractMapper3D*            m_mapper;
    };
}

#endif // !NavSlice3dActor_h 