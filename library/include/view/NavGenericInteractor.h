#ifndef NavGenericInteractor_h
#define NavGenericInteractor_h
#ifdef __linux__

#include <vtkGenericRenderWindowInteractor.h>

namespace fiui 
{
    class NavGenericInteractor : public vtkGenericRenderWindowInteractor 
    {
    public:
        vtkTypeMacro(NavGenericInteractor, vtkGenericRenderWindowInteractor);
        static NavGenericInteractor* New();
        void Initialize() override;
    protected:
        NavGenericInteractor() = default;
        virtual ~NavGenericInteractor() = default;
    };
}
#endif
#endif // !NavGenericInteractor_h