#ifndef NavView_h
#define NavView_h

#include <QQuickFramebufferObject>
#include <QMouseEvent>

#include <QVTKInteractorAdapter.h>

#include <vtkRenderWindowInteractor.h>
#include <vtkImageData.h>
#include <vtkCellPicker.h>
#include <vtkRenderer.h>
#include <vtkPropPicker.h>

#include "NavWindow.h"
#include "NavInteractorStyle.h"


namespace fiui 
{
    class NavRenderWindow;
    class NavActorManager;
    class NavSubViewManager;
    class NavView : public QObject, public QQuickFramebufferObject::Renderer 
    {
        Q_OBJECT
        friend class NavRenderWindow;
        friend class NavInteractorStyle;
    public:
        NavView(NavRenderWindow* win);
        ~NavView();
        void synchronize(QQuickFramebufferObject*) override;
        void render() override;
        QOpenGLFramebufferObject* createFramebufferObject(const QSize&) override;

        void initialise(QVariantMap& data);
        void removeActor(const QString& id);
        void removeAllActor();
        void removeStage(const QString& id);
        bool addActorToStage(const QString& id,const QString& stageId);
        void removeActorFromStage(const QString& id,const QString& stageId);

        QVariantMap getActorParams(const QString& id);
        QVariantMap getActorMap();
        QVariantMap getStageMap();
        QVariantList getLayoutValue();
        QVariantMap getStageLayoutMap();
        QVariantMap getActorStageMap();
        QVariantMap getStageActorMap();

        NavRenderWindow* getRenderWindow();
        NavWindow* getWindow();

        void pickEventHandle(quint64 prop,QString button,double* position);
    private:
        void handleEvent(NavViewEvent* event);
        void setLayout();

        NavRenderWindow*                        m_renderWindow;
        NavWindow*                              m_window;
        QOpenGLFramebufferObject*               m_fbo;
        QVTKInteractorAdapter*                  m_dapter;
        vtkCellPicker*                          m_picker;
        vtkRenderWindowInteractor*              m_interactor;
        NavInteractorStyle*                     m_interactorStyle;
        std::queue<std::shared_ptr<QEvent>>     m_qtEvents;
        NavActorManager*                        m_actorMgr;
        NavSubViewManager*                      m_subViewMgr;

        QVariantMap             m_mapActorStages;
        QVariantMap             m_mapStageActors;
        qint32                  m_layoutType;
    };
}

#endif // !NavView_h