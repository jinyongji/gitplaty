#ifndef NavInteractorStyle_h
#define NavInteractorStyle_h

#include <vtkInteractionStyleModule.h> // For export macro
#include <vtkInteractorStyle.h>
#include <vtkObjectFactory.h>

namespace fiui
{
    class NavView;
    class NavInteractorStyle : public vtkInteractorStyle
    {
    protected:
        NavInteractorStyle();
        virtual ~NavInteractorStyle() override;
    private:
        NavInteractorStyle(const NavInteractorStyle&) = delete;
        void operator=(const NavInteractorStyle&) = delete;
    public:
        vtkTypeMacro(NavInteractorStyle, vtkInteractorStyle);
        static NavInteractorStyle *New();
        void PrintSelf(ostream& os, vtkIndent indent) override;
        void SetView(NavView*);
        void OnMouseMove() override;
        void OnLeftButtonDown() override;
        void OnLeftButtonUp() override;
        void OnMiddleButtonDown() override;
        void OnMiddleButtonUp() override;
        void OnRightButtonDown() override;
        void OnRightButtonUp() override;
        void OnMouseWheelForward() override;
        void OnMouseWheelBackward() override;
        void OnFourthButtonDown() override;
        void OnFourthButtonUp() override;
        void OnFifthButtonDown() override;
        void OnFifthButtonUp() override;
        void OnChar() override;
    protected:
        void Rotate() override;
        void Spin() override;
        void Pan() override;
        void Dolly() override;
        void Zoom() override;
        virtual void Dolly(double factor);
    protected:
        double              m_MotionFactor;
        int                 m_iPickPt[2];
        bool                m_bLeftBtnPick;
        bool                m_bRightBtnPick;
        NavView*            m_viewPtr;
        void*               m_actionRenderer;
    };
}

#endif // !NavInteractorStyle_h