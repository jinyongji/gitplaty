#ifndef NavWindow_h
#define NavWindow_h

#include <memory>
#include <chrono>
#include <queue>
#include <QThread>
#include <QQuickFramebufferObject>
#include "../NavViewGlobal.h"
#include <QQuickWindow>

class vtkGenericOpenGLRenderWindow;
namespace fiui
{
    class NavView;
    class NavWindow : public QQuickFramebufferObject
    {
        Q_OBJECT
        Q_PROPERTY(quint32 layoutType READ layoutType WRITE setLayoutType)
        Q_PROPERTY(QVariantMap initialiseMap READ initialiseMap WRITE setInitialiseMap)
    public:
        NavWindow();
        ~NavWindow();
        QQuickFramebufferObject::Renderer *createRenderer() const override;
        virtual void init();
        void update();

        std::queue<std::shared_ptr<QEvent>>* getQtEventsList();
        std::queue<std::shared_ptr<NavViewEvent>>* getUserEventsList();
        vtkGenericOpenGLRenderWindow* GetRenderWindow();
                                                                           
        quint32 layoutType();
        void setLayoutType(const quint32 layoutType);
        QVariantMap initialiseMap();
        void setInitialiseMap(const QVariantMap& initialise);
    signals:
        void errorMsg(const QString&);
        void viewEvent(const QVariant&);

    public slots:
        void addActor(const QString& type,const QString& userType,const QString& id,const QVariantMap& map);
        void removeActor(const QString& id);
        void modifyActor(const QString& id,const QVariantMap& map);
        void modifyActorData(const QString& id,const QVariantMap& map);
        void removeAllActor();
        // void modifyPosition(const QString& type,const QString& id,const QVariantList& map);//todo

        void addStage(const QString& id,const QString& viewType,const QString& direction);
        void modifyStage(const QString& id,const QString& viewType,const QString& direction);
        void removeStage(const QString& id);
        void resetStageCamera(const QString& id);

        void setLayoutValue(const QVariantList& layout);

        void addStageToLayout(const QString& stageId,const quint32 id);
        void removeStageFromLayout(const QString& stageId);

        void addActorToStage(const QString& id,const QString& stageId);
        void removeActorFromStage(const QString& id,const QString& stageId);

        Q_INVOKABLE QVariantMap getActorParams(const QString& id);
        Q_INVOKABLE QVariantMap getActorMap();
        Q_INVOKABLE QVariantMap getStageMap();
        Q_INVOKABLE QVariantList getLayoutValue();
        Q_INVOKABLE QVariantMap getStageLayoutMap();
        Q_INVOKABLE QVariantMap getActorStageMap();
        Q_INVOKABLE QVariantMap getStageActorMap();
    protected:
        bool eventFilter(QObject *object, QEvent *evt) override;
    private:
        vtkGenericOpenGLRenderWindow*                   m_win;
        std::queue<std::shared_ptr<QEvent>>             m_qtEvents;
        std::queue<std::shared_ptr<NavViewEvent>>       m_userEvents;
        bool                                            m_initialized;
        mutable NavView*                                m_view;
        quint32                                         m_layoutType;
    };
    
}

#endif // !NavWindow_h