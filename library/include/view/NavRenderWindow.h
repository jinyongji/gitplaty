#ifndef NavRenderWindow_h
#define NavRenderWindow_h

#include <QOpenGLFunctions>
#include <QOpenGLFramebufferObject>

#include <vtkExternalOpenGLRenderWindow.h>

namespace fiui 
{
    class NavRenderWindow : public vtkExternalOpenGLRenderWindow, protected QOpenGLFunctions 
    {
    public:
        static NavRenderWindow* New();
        virtual void OpenGLInitState() override;
        void InternalRender();
        void SetFramebufferObject(QOpenGLFramebufferObject*);
    protected:
        NavRenderWindow();
        ~NavRenderWindow();
    };
}

#endif // !NavRenderWindow_h