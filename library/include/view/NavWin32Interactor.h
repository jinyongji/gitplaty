#ifndef NavWin32Interactor_h
#define NavWin32Interactor_h

#ifdef _MSC_VER

#include <vtkWin32RenderWindowInteractor.h>

namespace fiui 
{
    class NavWin32Interactor : public vtkWin32RenderWindowInteractor 
    {
    public:
        vtkTypeMacro(NavWin32Interactor, vtkWin32RenderWindowInteractor);
        static NavWin32Interactor* New();
        void Initialize() override;
    protected:
        NavWin32Interactor() = default;
        virtual ~NavWin32Interactor() = default;
    };
}

#endif
#endif // NavWin32Interactor_h