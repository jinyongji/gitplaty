#ifndef NavStage_h
#define NavStage_h
#include <vtkRenderer.h>
#include <QString>

namespace fiui
{
    class NavActor;
    class NavSubView;
    class NavStage
    {
        friend class NavSubView;
    public:
        NavStage(const QString& viewType, const QString& direction);
        ~NavStage();
        NavStage(const NavStage&) = delete;
        void operator=(const NavStage&) = delete;

        void AddActor(NavActor*);
        void RemoveActor(NavActor*);

        void SetViewType(const QString& viewType);
        void SetDirection(const QString& direction);
        QString GetViewType();
        QString GetDirection();
        void ResetCamera();
        bool HasRenderer(vtkRenderer*);
    private:
        vtkRenderer*    m_renderer;
        QString         m_viewType;
        QString         m_direction;
    };
}

#endif // !NavStage_h