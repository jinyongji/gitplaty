#ifndef NavSubView_h
#define NavSubView_h
#include <array>
#include <QString>
#include "NavStage.h"
#include <QVariantList>

namespace fiui
{
    class NavSubView
    {
    public:
        NavSubView(const QVariantList& layout,void* parent);
        ~NavSubView();
        NavSubView(const NavSubView&) = delete;
        void operator=(const NavSubView&) = delete;

        void SetLayout(QVariantList&);
        QVariantList GetLayout();

        void SetStage(NavStage*);
        void RemoveStage();

        void SetStageOpacity();
    private:
        QVariantList            m_layout;
        NavStage*               m_stage;
        void*                   m_parentWindow;
    };
}

#endif // !NavSubView_h