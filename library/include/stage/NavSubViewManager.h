#ifndef NavSubViewManager_h
#define NavSubViewManager_h
#include "../NavViewGlobal.h"
#include "NavStage.h"
#include "NavSubView.h"
#include "../view/NavRenderWindow.h"
#include "../view/NavView.h"

namespace fiui
{
    class NavSubViewManager
    {
    public:
        NavSubViewManager(NavView* parent);
        ~NavSubViewManager();
        NavSubViewManager(const NavSubViewManager&) = delete;
        void operator=(const NavSubViewManager&) = delete;

        void handleEvent(NavViewEvent* event);

        quint32 layoutType();
        bool setLayoutType(const quint32 layoutType);
        quint32 getLayoutType();
        bool setLayoutValue(const QVariantList& layout);
        QVariantList getLayoutValue();
        bool addStage(const QString& id,const QString& viewType,const QString& direction);
        void modifyStage(const QString& id,const QString& viewType,const QString& direction);
        void removeStage(const QString& id);
        QVariantMap getStage();
        QVariantMap getStageLayoutMap();
        
        void resetStageCamera(const QString& id);

        bool addStageToLayout(const QString& id,const quint32 stageId);
        void removeStageFromLayout(const QString& id);

        NavStage* getStage(const QString& id);
        NavStage* findStage(quint64 renderer);
    private:
        void InnerSetLayoutValue(const QVariantList& layout);
        QMap<QString,NavStage*>         m_mapStage;
        std::vector<NavSubView*>        m_subViews;
        QMap<QString,quint32>           m_mapStageSubView;
        QMap<quint32,QString>           m_mapSubViewStage;
        LayoutType                      m_layoutType;
        NavView*                        m_parent;
        NavRenderWindow*                m_renderWindow;
        QVariantList                    m_fourWindow;
        QVariantList                    m_threeWindows;
        vtkRenderer*                    m_bottomRenderer;

    };
    
}

#endif // !NavSubView_h