﻿import QtQuick 2.0
/**@
     filename   FinavConfig.qml
     brief      定义Finav的各种属性
     version    1.0
     author     xh
     date       2021-01-22
     copyright  重庆博仕康科技有限公司
*/
QtObject{
    /**
        @brief  定义布局的类型
    */
    enum LayoutType{

        SingleType = 0x01,
        LeftRightType = 0x02,
        TopBottomType = 0x03,
        OneToTwoType = 0x04,
        OneToThreeType = 0x05,
        TwoToTwoType = 0x06
    }
    /**
        @brief  定义注册方式的类型
    */
    enum RegisterType{

        ManualRegistration = 0x01,
        AutoRegistration= 0x02,
        CArmRegistration= 0x03,
        Registration = 0x04
    }
}
