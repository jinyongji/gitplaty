﻿import QtQuick 2.0
import QtQuick.Controls 2.5
import Fiui 1.0
import "../dialog"
import "../"
/**@
     filename   Title.qml
     brief      Finav导航界面的标题栏
     version    1.0
     author     xh
     date       2021-01-28
     copyright  重庆博仕康科技有限公司
*/
Item{

    id: id_root
    /**
        @brief  选择固定常用提示语
    */
    property int index: 0
    Rectangle{

        id:id_background
        width: parent.width
        height: parent.height
        color: "#363638"
    }
    Row{

        id: id_layoutGrid
        anchors.verticalCenter: id_root.verticalCenter
        anchors.left: id_root.left
        anchors.leftMargin: horSpacing(199)
        height: horSpacing(13)
        spacing: horSpacing(13)
        FiIconButton{

            id: id_singleType
            btnNormalSrc: "qrc:/Finav/image/SingleType_normal.svg"
            btnHoverSrc: "qrc:/Finav/image/SingleType_hover.svg"
            btnPressedSrc: "qrc:/Finav/image/SingleType_pressed.svg"
            btnCheckedSrc: "qrc:/Finav/image/SingleType_hover.svg"
            checkable: true
            width: horSpacing(13)
            height: horSpacing(13)
            onClicked: {

                layoutType = FinavConfigure.LayoutType.SingleType
            }
        }
        FiIconButton{

            id: id_leftRightType
            btnNormalSrc: "qrc:/Finav/image/LeftRightType_normal.svg"
            btnHoverSrc: "qrc:/Finav/image/LeftRightType_hover.svg"
            btnPressedSrc: "qrc:/Finav/image/LeftRightType_pressed.svg"
            btnCheckedSrc: "qrc:/Finav/image/LeftRightType_hover.svg"
            checkable: true
            width: horSpacing(13)
            height: horSpacing(13)
            onClicked: {

                layoutType = FinavConfigure.LayoutType.LeftRightType
            }
        }
        FiIconButton{

            id: id_topBottomType
            btnNormalSrc: "qrc:/Finav/image/TopBottomType_normal.svg"
            btnHoverSrc: "qrc:/Finav/image/TopBottomType_hover.svg"
            btnPressedSrc: "qrc:/Finav/image/TopBottomType_pressed.svg"
            btnCheckedSrc: "qrc:/Finav/image/TopBottomType_hover.svg"
            checkable: true
            width: horSpacing(13)
            height: horSpacing(13)
            onClicked: {

                layoutType = FinavConfigure.LayoutType.TopBottomType
            }
        }
        FiIconButton{

            id: id_oneToTwoType
            btnNormalSrc: "qrc:/Finav/image/OneToTwoType_normal.svg"
            btnHoverSrc: "qrc:/Finav/image/OneToTwoType_hover.svg"
            btnPressedSrc: "qrc:/Finav/image/OneToTwoType_pressed.svg"
            btnCheckedSrc: "qrc:/Finav/image/OneToTwoType_hover.svg"
            checkable: true
            width: horSpacing(13)
            height: horSpacing(13)
            onClicked: {

                layoutType = FinavConfigure.LayoutType.OneToTwoType
            }
        }
        FiIconButton{

            id: id_oneToThreeType
            btnNormalSrc: "qrc:/Finav/image/OneToThreeType_normal.svg"
            btnHoverSrc: "qrc:/Finav/image/OneToThreeType_hover.svg"
            btnPressedSrc: "qrc:/Finav/image/OneToThreeType_pressed.svg"
            btnCheckedSrc: "qrc:/Finav/image/OneToThreeType_hover.svg"
            checkable: true
            width: horSpacing(13)
            height: horSpacing(13)
            onClicked: {

                layoutType = FinavConfigure.LayoutType.OneToThreeType
            }
        }
        FiIconButton{

            id: id_twoToTwoType
            btnNormalSrc: "qrc:/Finav/image/TwoToTwoType_normal.svg"
            btnHoverSrc: "qrc:/Finav/image/TwoToTwoType_hover.svg"
            btnPressedSrc: "qrc:/Finav/image/TwoToTwoType_pressed.svg"
            btnCheckedSrc: "qrc:/Finav/image/TwoToTwoType_hover.svg"
            checkable: true
            checked: true
            width: horSpacing(13)
            height: horSpacing(13)
            onClicked: {

                layoutType = FinavConfigure.LayoutType.TwoToTwoType
            }
            Component.onCompleted: _updateState()
        }
    }
    ButtonGroup{

        id: id_btnGroup
        buttons: id_layoutGrid.children
    }
    FiIconButton{

        id: id_logo
        anchors.verticalCenter: id_root.verticalCenter
        anchors.left: id_root.left
        anchors.leftMargin: horSpacing(12)
        width: horSpacing(60)
        height: verSpacing(24)
        btnNormalSrc: "qrc:/Finav/image/LOGO_normal.svg"
        btnHoverSrc: "qrc:/Finav/image/LOGO_hover.svg"
        btnPressedSrc: "qrc:/Finav/image/LOGO_pressed.svg"
        onClicked: id_about.open()
        About{

            id: id_about
            width: horSpacing(820)
            height: verSpacing(520)
        }
    }
    Rectangle{

        id: id_tipsBackground
        anchors.centerIn: id_root
        width: horSpacing(285)
        height: verSpacing(25)
        radius: 10
        color: "#29282d"
        FiText {

            id: id_tips
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: horSpacing(28)
            font.pixelSize: horSpacing(12)
            text: qsTr($MainClue[index])
        }
    }
    Timer{

        id: id_tipsTimer
        interval: 2000
        onTriggered: {

            id_tips.text = qsTr($MainClue[index])
            id_tips.color = "white"
        }
    }
    /**
        @brief  是否点击冻结
    */
    property bool frozenState: false
    FiIconButton{

        id: id_unfrozen
        width: horSpacing(13)
        height: verSpacing(15)
        anchors.verticalCenter: id_root.verticalCenter
        anchors.left: id_tipsBackground.right
        anchors.leftMargin: horSpacing(37)
        btnNormalSrc: frozenState ? "qrc:/Finav/image/frozen_normal.svg" : "qrc:/Finav/image/unfrozen_normal.svg"
        btnHoverSrc: frozenState ? "qrc:/Finav/image/frozen_hover.svg"  : "qrc:/Finav/image/unfrozen_hover.svg"
        btnPressedSrc: frozenState ? "qrc:/Finav/image/frozen_pressed.svg"  : "qrc:/Finav/image/unfrozen_presssed.svg"
        onClicked: {

            _changeFrozenState()
        }
    }
    FiIconButton{

        id: id_warning
        anchors.verticalCenter: id_root.verticalCenter
        anchors.left: id_tipsBackground.right
        anchors.leftMargin: horSpacing(60)
        width: horSpacing(15)
        height: verSpacing(17)
        btnNormalSrc: "qrc:/Finav/image/warning_normal.svg"
        btnHoverSrc: "qrc:/Finav/image/warning_hover.svg"
        btnPressedSrc: "qrc:/Finav/image/warning_pressed.svg"
    }
    FiText {

        id: id_time
        anchors.verticalCenter: id_root.verticalCenter
        anchors.right: id_root.right
        anchors.rightMargin: horSpacing(121)
        height: horSpacing(16)
        width: verSpacing(72)
        font.pixelSize: horSpacing(12)
    }
    Timer{

        id: id_timer
        interval: 100
        running: true
        repeat: true
        onTriggered: {

            id_time.text = Qt.formatDateTime(new Date,"yyyy.MM.dd  hh:mm:ss")
        }
    }
    FiIconButton{

        id: id_close
        anchors.verticalCenter: id_root.verticalCenter
        anchors.right: id_root.right
        anchors.rightMargin: horSpacing(19)
        width: horSpacing(20)
        height: verSpacing(21)
        btnNormalSrc: "qrc:/Finav/image/off_normal.svg"
        btnHoverSrc: "qrc:/Finav/image/off_hover.svg"
        btnPressedSrc: "qrc:/Finav/image/off_pressed.svg"
        onClicked: Qt.quit()
    }
    Component.onCompleted: {


        id_homeScreen.tipsInfo.connect(_setTipsInfo)
        id_homeScreen.tipsUpdate.connect(_tipsUpdate)
    }

    function _changeFrozenState(){

        if(frozenState)

            frozenState = false
        else
            frozenState = true
    }
    function _setTipsInfo(info){

        id_tips.text = info[0]
        id_tipsTimer.interval = info[1]
        id_tipsTimer.start()
        if(info[2] === undefined)
            id_tips.color = "white"
        else
            id_tips.color = info[2]
    }
    function _tipsUpdate(idx){

        index = idx
        id_tipsTimer.interval = 100
        id_tipsTimer.start()
    }
}
