﻿import QtQuick 2.0
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.0
import Fiui 1.0
import "../integration"
/**@
     filename   LeftToolBar.qml
     brief      左侧工具栏
     version    1.0
     author     xh
     date       2021-01-28
     copyright  重庆博仕康科技有限公司
*/

Item{

    id: id_root
    Rectangle{

        id: lefttoolbar
        anchors.fill: parent
        color: "#313133"
    }
    FiBtnPopup{

        id: id_polaris
        anchors.horizontalCenter: id_root.horizontalCenter
        anchors.top: id_root.top
        anchors.topMargin: verSpacing(14)
        xOffset: horSpacing(22)
        width: horSpacing(21)
        height: verSpacing(24)
        btnNormalSrc: "qrc:/Finav/image/optical_normal.svg"
        btnHoverSrc: "qrc:/Finav/image/optical_hover.svg"
        btnPressedSrc: "qrc:/Finav/image/optical_pressed.svg"
        popupSrc: "qrc:/Finav/image/optcontentbg.svg"
        switchVisible: true
        distSrc1:"qrc:/Finav/image/distance.svg"
        sliderSrc:"qrc:/Finav/image/opt_slider.svg"
    }
    Column{

        id: id_polaristools
        anchors.horizontalCenter: id_root.horizontalCenter
        anchors.top: id_root.top
        anchors.topMargin: verSpacing(55)
        width: horSpacing(36)
        spacing: verSpacing(26)
        ButtonListPopup{

            id: id_toolA
            width: horSpacing(36)
            height: verSpacing(36)
            xOffset:  horSpacing(30)
            btnNormalSrc: "qrc:/Finav/image/toolA"
            btnHoverSrc: "qrc:/Finav/image/toolA"
            btnPressedSrc: "qrc:/Finav/image/toolA"
            popupSrc: "qrc:/Finav/image/optcontentbg.svg"
            xlistOffset:  horSpacing(30)
            listBottomMargin: horSpacing(20)
            contentRadius: horSpacing(10)
            toolname: qsTr("UDG")
            toolVisible: true
            toolkitSrc: "qrc:/Finav/image/UDG.png"
            toolkitSrc2:"qrc:/Finav/image/dynamicTracking .png"
            btnText.text: qsTr("探针")
            btnText.font.pixelSize: horSpacing(9)
            btnText.anchors{
                top: id_toolA.bottom
                horizontalCenter: id_toolA.horizontalCenter
                topMargin: verSpacing(5)
            }
        }
        ButtonListPopup{

            id: id_toolB
            width: horSpacing(36)
            height: verSpacing(36)
            btnNormalSrc: "qrc:/Finav/image/toolB"
            btnHoverSrc: "qrc:/Finav/image/toolB"
            btnPressedSrc: "qrc:/Finav/image/toolB"
            popupSrc: "qrc:/Finav/image/optcontentbg.svg"
            xOffset: horSpacing(30)
            xlistOffset:  horSpacing(30)
            listBottomMargin: horSpacing(20)
            contentRadius: 10
            toolname: qsTr("UDG")
            toolkitSrc: "qrc:/Finav/image/UDG.png"
            toolVisible: true
            btnText.text: qsTr("穿刺针")
            btnText.font.pixelSize: horSpacing(9)
            btnText.anchors{

                top: id_toolB.bottom
                horizontalCenter: id_toolB.horizontalCenter
                topMargin: verSpacing(5)
            }
        }
        ButtonListPopup{

            id: id_toolC
            width: horSpacing(36)
            height: verSpacing(36)
            btnNormalSrc: "qrc:/Finav/image/toolC"
            btnHoverSrc: "qrc:/Finav/image/toolC"
            btnPressedSrc: "qrc:/Finav/image/toolC"
            popupSrc: "qrc:/Finav/image/optcontentbg.svg"
            xOffset: horSpacing(30)
            xlistOffset:  horSpacing(30)
            listBottomMargin: horSpacing(20)
            toolVisible: true
            toolname: qsTr("UDG")
            toolkitSrc: "qrc:/Finav/image/UDG.png"
            btnText.text: qsTr("探针")
            btnText.font.pixelSize: horSpacing(9)
            btnText.anchors{

                top: id_toolC.bottom
                horizontalCenter: id_toolC.horizontalCenter
                topMargin: verSpacing(5)
            }
        }
        ButtonListPopup{

            id: id_toolD
            width: horSpacing(36)
            height: verSpacing(36)
            btnNormalSrc: "qrc:/Finav/image/toolD"
            btnHoverSrc: "qrc:/Finav/image/toolD"
            btnPressedSrc: "qrc:/Finav/image/toolD"
            popupSrc: "qrc:/Finav/image/optcontentbg.svg"
            xOffset: horSpacing(30)
            xlistOffset:  horSpacing(30)
            listBottomMargin: horSpacing(20)
            toolVisible: true
            btnText.text: qsTr("穿刺针")
            toolname: qsTr("UDG")
            toolkitSrc: "qrc:/Finav/image/UDG.png"
            btnText.font.pixelSize: horSpacing(9)
            btnText.anchors{

                top: id_toolD.bottom
                horizontalCenter: id_toolD.horizontalCenter
                topMargin: verSpacing(5)
            }
        }
    }
    FiBtnPopup{

        id: id_aurora
        anchors.horizontalCenter: id_root.horizontalCenter
        anchors.top: id_root.top
        anchors.topMargin: verSpacing(344)
        width: horSpacing(40)
        height: verSpacing(18)
        btnNormalSrc: "qrc:/Finav/image/electromagnetic_normal.svg"
        btnHoverSrc: "qrc:/Finav/image/electromagnetic_hover.svg"
        btnPressedSrc: "qrc:/Finav/image/electromagnetic_pressed.svg"
        popupSrc: "qrc:/Finav/image/optcontentbg.svg"
        distSrc1:"qrc:/Finav/image/eledistance.svg"
        sliderSrc:"qrc:/Finav/image/opt_slider.svg"
        xOffset: horSpacing(22)
        tipsVisible:false
    }
    Column{

        id: id_auroratools
        anchors.horizontalCenter: id_root.horizontalCenter
        anchors.top: id_root.top
        anchors.topMargin: verSpacing(389)
        width: horSpacing(36)
        spacing: verSpacing(26)
        ButtonListPopup{

            id: id_tool1
            width: horSpacing(36)
            height: verSpacing(36)
            btnNormalSrc: "qrc:/Finav/image/tool1"
            btnHoverSrc: "qrc:/Finav/image/tool1"
            btnPressedSrc: "qrc:/Finav/image/tool1"
            popupSrc: "qrc:/Finav/image/optcontentbg.svg"
            popupRadius1: horSpacing(5)
            xOffset: horSpacing(30)
            xlistOffset:  horSpacing(30)
            listBottomMargin: horSpacing(20)
            toolVisible: true
            btnText.text: qsTr("探针")
            toolname: qsTr("小黑片")
            toolkitSrc: "qrc:/Finav/image/littleBlack piece.png"
            btnText.font.pixelSize: horSpacing(9)
            btnText.anchors{

                top: id_tool1.bottom
                horizontalCenter: id_tool1.horizontalCenter
                topMargin: verSpacing(5)
            }
        }
        ButtonListPopup{

            id: id_tool2
            width: horSpacing(36)
            height: verSpacing(36)
            btnNormalSrc: "qrc:/Finav/image/tool2"
            btnHoverSrc: "qrc:/Finav/image/tool2"
            btnPressedSrc: "qrc:/Finav/image/tool2"
            popupSrc: "qrc:/Finav/image/optcontentbg.svg"
            xOffset: horSpacing(30)
            xlistOffset:  horSpacing(30)
            listBottomMargin: horSpacing(20)
            toolVisible: true
            btnText.text: qsTr("穿刺针")
            toolname: qsTr("小黑片")
            toolkitSrc: "qrc:/Finav/image/littleBlack piece.png"
            btnText.font.pixelSize: horSpacing(9)
            btnText.anchors{

                top: id_tool2.bottom
                horizontalCenter: id_tool2.horizontalCenter
                topMargin: verSpacing(5)
            }
        }
        ButtonListPopup{

            id: id_tool3
            width: horSpacing(36)
            height: verSpacing(36)
            btnNormalSrc: "qrc:/Finav/image/tool3"
            btnHoverSrc: "qrc:/Finav/image/tool3"
            btnPressedSrc: "qrc:/Finav/image/tool3"
            popupRadius1: horSpacing(5)
            popupSrc:  {
                if ( popupHeight + id_tool3.y +id_auroratools.y < Screen.height)
                "qrc:/Finav/image/optcontentbg.svg"
                else
               "qrc:/Finav/image/optcontentbg2.svg"
                }
            xOffset: horSpacing(30)
            yOffset:  {
                if ( popupHeight + id_tool3.y +id_auroratools.y < Screen.height)
                -verSpacing(35)
                else
                -verSpacing(200)
                }
            xlistOffset:  horSpacing(30)
            listBottomMargin: horSpacing(20)
            toolVisible: true
            btnText.text: qsTr("探针")
            toolname: qsTr("小黑片")
            toolkitSrc: "qrc:/Finav/image/littleBlack piece.png"
            btnText.font.pixelSize: horSpacing(9)
            btnText.anchors{

                top: id_tool3.bottom
                horizontalCenter: id_tool3.horizontalCenter
                topMargin: verSpacing(5)
            }
        }
        ButtonListPopup{

            id: id_tool4
            width: horSpacing(36)
            height: verSpacing(36)
            btnNormalSrc: "qrc:/Finav/image/tool4"
            btnHoverSrc: "qrc:/Finav/image/tool4"
            btnPressedSrc: "qrc:/Finav/image/tool4"
            popupRadius1: horSpacing(5)
            popupSrc:  {
                if ( popupHeight + id_tool4.y +id_auroratools.y < Screen.height)
                "qrc:/Finav/image/optcontentbg.svg"
                else
               "qrc:/Finav/image/optcontentbg2.svg"
                }
            xOffset: horSpacing(30)
            yOffset:  {
                if ( popupHeight + id_tool4.y +id_auroratools.y < Screen.height)
                -verSpacing(35)
                else
                -verSpacing(200)
                }
            xlistOffset:  horSpacing(30)
            listBottomMargin: horSpacing(20)
            toolVisible: true
            btnText.text: qsTr("穿刺针")
            toolname: qsTr("小黑片")
            toolkitSrc: "qrc:/Finav/image/littleBlack piece.png"
            btnText.font.pixelSize: horSpacing(9)
            btnText.anchors{

                top: id_tool4.bottom
                horizontalCenter: id_tool4.horizontalCenter
                topMargin: verSpacing(5)
            }
        }
    }
}
