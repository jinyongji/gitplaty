﻿import QtQuick 2.0
import QtQuick.Dialogs 1.3
import QtQuick.Window 2.12
import QtQuick.Controls 2.5
import Fiui 1.0
import "../dialog"
/**@
     filename   RightControlBar.qml
     brief      右侧功能栏
     version    1.0
     author     xh
     date       2021-01-28
     copyright  重庆博仕康科技有限公司
*/
Item{

    id: id_root
    /**
        @brief 截图默认保存路径
    */
    property string screenshotPath: "./screenshot/"
    Rectangle{

        id: rightcontrolbar
        anchors.fill: parent
        color: "#313133"
    }
    Column{

        id: id_column
        anchors.horizontalCenter: id_root.horizontalCenter
        anchors.top: id_root.top
        anchors.topMargin: verSpacing(25)
        height: verSpacing(23)
        spacing: verSpacing(40)
        FiIconButton{

            id: id_newBtn
            width: horSpacing(20)
            height: verSpacing(22)
            btnNormalSrc: "qrc:/Finav/image/new_normal.svg"
            btnHoverSrc: "qrc:/Finav/image/new_hover.svg"
            btnPressedSrc: "qrc:/Finav/image/new_pressed.svg"
            btnText.text: qsTr("新建")
            btnText.font.pixelSize: horSpacing(9)
            btnText.anchors{

                top: id_newBtn.bottom
                horizontalCenter: id_newBtn.horizontalCenter
                topMargin: verSpacing(5)
            }
            onClicked: {

                id_modeLoader.sourceComponent = id_registerMode
                id_modeLoader.item.open()
            }
        }
        FiIconButton{

            id: id_planBtn
            width: horSpacing(23)
            height: verSpacing(22)
            btnNormalSrc: "qrc:/Finav/image/plan_normal.svg"
            btnHoverSrc:"qrc:/Finav/image/plan_hover.svg"
            btnPressedSrc:"qrc:/Finav/image/plan_plan_pressed.svg"
            btnText.text: qsTr("计划")
            btnText.font.pixelSize: horSpacing(9)
            btnText.anchors{

                top: id_planBtn.bottom
                horizontalCenter: id_planBtn.horizontalCenter
                topMargin: verSpacing(5)
            }
       }
        FiIconButton{

            id: id_measureBtn
            width: verSpacing(21)
            height: verSpacing(16)
            btnNormalSrc: "qrc:/Finav/image/measurement_normal.svg"
            btnHoverSrc: "qrc:/Finav/image/measurement_hover.svg"
            btnPressedSrc: "qrc:/Finav/image/measurement_pressed.svg"
            btnText.text: qsTr("测量")
            btnText.font.pixelSize: horSpacing(9)
            btnText.anchors{

                top: id_measureBtn.bottom
                horizontalCenter: id_measureBtn.horizontalCenter
                topMargin: verSpacing(5)
            }
        }
        FiIconButton{

            id: id_nailBtn
            anchors.horizontalCenter: parent.horizontalCenter
            width: horSpacing(18)
            height: verSpacing(25)
            btnNormalSrc: "qrc:/Finav/image/nailing_normal.svg"
            btnHoverSrc:"qrc:/Finav/image/nailing_hover.svg"
            btnPressedSrc:"qrc:/Finav/image/nailing_pressed.svg"
            btnText.text: qsTr("置钉")
            btnText.font.pixelSize: horSpacing(9)
            btnText.anchors{

                top: id_nailBtn.bottom
                horizontalCenter: id_nailBtn.horizontalCenter
                topMargin: verSpacing(5)
            }
        }
        FiIconButton{

            id: id_screenshotBtn
            anchors.horizontalCenter: parent.horizontalCenter
            width: horSpacing(20)
            height: verSpacing(18)
            btnNormalSrc: "qrc:/Finav/image/screenshots_normal.svg"
            btnHoverSrc:"qrc:/Finav/image/screenshots_hover.svg"
            btnPressedSrc:"qrc:/Finav/image/screenshots_pressed.svg"
            btnText.text: qsTr("截图")
            btnText.font.pixelSize: horSpacing(9)
            btnText.anchors{

                top: id_screenshotBtn.bottom
                horizontalCenter: id_screenshotBtn.horizontalCenter
                topMargin: verSpacing(5)
            }
            onClicked: _screenshot()
         }
        FiIconButton{

            id: id_videoBtn
            width: horSpacing(20)
            height: verSpacing(14)
            btnNormalSrc: "qrc:/Finav/image/video_normal.svg"
            btnHoverSrc:"qrc:/Finav/image/video_hover.svg"
            btnPressedSrc:"qrc:/Finav/image/video_pressed.svg"
            btnText.text: qsTr("录像")
            btnText.font.pixelSize: horSpacing(9)
            btnText.anchors{

                top: id_videoBtn.bottom
                horizontalCenter: id_videoBtn.horizontalCenter
                topMargin: verSpacing(5)
            }

        }
    }
    function _screenshot(){

        var date = Qt.formatDateTime(new Date,"yyyy_MM_dd_hh_mm_ss")
        var picName = date + ".png"
        id_homeScreen.grabToImage(function(result){

            if(result.saveToFile(screenshotPath + picName)){

                tipsInfo([qsTr("截图成功"),2000])
            }
            else{

                tipsInfo([qsTr("截图失败"),2000,"yellow"])
            }
        })
    }
}

