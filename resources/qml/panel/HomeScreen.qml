﻿import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Controls.Styles 1.4
import QtQuick.Window 2.2
import Qt.labs.platform 1.1
import com.locator 1.0
import com.locateEvent 1.0
import com.location 1.0
import com.position 1.0
import Fiui 1.0
import "../integration"
import "../dialog"
import ".."
/**@
     filename   HomeScreen.qml
     brief      Finav导航界面的主界面
     version    1.0.1
     author     xh
     date       2021-01-28/2021-02-20
     copyright  重庆博仕康科技有限公司
*/

Rectangle{

    id: id_homeScreen
    signal tipsInfo(var info)
    signal tipsUpdate(int idx)
    color: "#1F1C1F"
    /**
        @brief 标题栏
    */
    Title{

        id: id_title
        anchors.left: parent.left
        anchors.right: parent.right
        width: id_homeScreen.width
        height: verSpacing(35)
    }
    /**
        @brief 调整窗宽
    */
    AdjustWidthLevel{

        id: id_winWidth
        anchors.top: id_title.bottom
        anchors.topMargin: horSpacing(10)
        anchors.left: id_homeScreen.left
        anchors.leftMargin: horSpacing(214)
        width: horSpacing(270)
        height: verSpacing(13)
        textName: "窗宽"
    }
    /**
        @brief 调整窗位
    */
    AdjustWidthLevel{

        id: id_winLevel
        anchors.top: id_title.bottom
        anchors.topMargin: horSpacing(10)
        anchors.right: id_homeScreen.right
        anchors.rightMargin:verSpacing(214)
        width: horSpacing(270)
        height: verSpacing(13)
        textName: "窗位"
    }
    /**
        @brief  调整阈值(暂时)
    */
    FiSlider{

        id: id_threshold
        anchors.top: id_title.bottom
        anchors.topMargin: horSpacing(16)
        anchors.horizontalCenter: parent.horizontalCenter
        width: horSpacing(150)
        height: verSpacing(2)
        minValue: 0
        maxValue: 2000
        sliderWidSize: horSpacing(10)
        sliderHeiSize: verSpacing(10)
        sliderStyleSrc: "qrc:/Finav/image/sliderbar.svg"
        FiText{

            id: thresholdText
            color: "#A4A4A4"
            font.pixelSize: horSpacing(9)
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.right
            anchors.leftMargin: horSpacing(10)
            text: id_threshold.sliderValue
        }
    }
    /**
        @brief 左侧工具栏
    */
    LeftToolbar{

        id: id_leftToolbar
        width: horSpacing(59)
        anchors.top: id_title.bottom
        anchors.topMargin: verSpacing(31)
        anchors.left: parent.left
        anchors.bottom: parent.bottom
    }
    /**
        @brief 右侧功能栏
    */
    RightFunctionBar{

        id: id_rightFunctionBar
        width: horSpacing(59)
        anchors.top: id_title.bottom
        anchors.topMargin: verSpacing(31)
        anchors.right: parent.right
        anchors.bottom: parent.bottom
    }
    property alias layoutType: id_view.layoutType
    /**
        @brief 视图
    */
    FinavViewManager{

        id: id_view
        anchors.bottom: parent.bottom
        anchors.left: id_leftToolbar.right
        anchors.right: id_rightFunctionBar.left
        height: id_leftToolbar.height
        offset: horSpacing(4)
        widLevel: id_winLevel.value
        widWidth: id_winWidth.value
        threshold: id_threshold.sliderValue
    }
    Locator{

        id: id_locator
    }

    /**
        @brief  选择注册模式
    */
    Loader{

        id: id_modeLoader
        onLoaded: item.chooseRegisterType.connect(_getRegistMode)
    }
    Component{

        id: id_registerMode
        RegisterMode{

            width: horSpacing(820)
            height: verSpacing(520)
        }
    }
    /**
        @brief  配准注册
    */
    Loader{

        id: id_registLoader
        onLoaded: item.sendResult.connect(_getMatrix)
    }
    Component{

        id: id_registration
        Registration{

            width: horSpacing(1072)
            height: verSpacing(715)
        }
    }
    /**
        @brief  文件选择器（暂时）
    */
    FolderDialog{

        id: id_fileDialog
        title: qsTr("选择DICOM文件路径")
        options: FolderDialog.ShowDirOnly
        onAccepted: {

            var filePath = id_fileDialog.folder.toString()
            filePath = filePath.substring(7)
            _loadDicomFile(filePath)
        }
    }
    FiButton{

        width: 50
        height: 50
        onClicked: {

            var STLInfo = []
            STLInfo.push(settings["kits"]["kit_id1"]["equipments"]["tools"]["p_screwtip"]["name"])
            STLInfo.push(settings["kits"]["kit_id1"]["equipments"]["tools"]["p_screwtip"]["stlPath"])
            id_view._testSTL(STLInfo[0],STLInfo[1])
        }
    }
    Component.onCompleted: {

//        _addDevices()
//        id_locator.locateUpdate.connect(id_view._getLocate)
    }
    function _addDevices(){

        var locateDevices = settings["devices"]["locateDevices"]
        var trackTools = settings["kits"]["kit_id1"]["equipments"]["trackTools"]
        var trackers = settings["kits"]["kit_id1"]["equipments"]["trackers"]
        var connectPoints = settings["kits"]["kit_id1"]["equipments"]["entries"]["configure"]["connectPoints"]
        var romPaths = []
        var entries = settings["kits"]["kit_id1"]["equipments"]["entries"]
        for(var entry in entries){

            var points = []
            entries[entry]["connectPoints"].forEach(function(i){
                points.push(i["device"])
            })
//            id_locator.addLocationEntry(entry,points)
        }
        for( var key in locateDevices){

            id_locator.addDevice(key,locateDevices[key]["deviceType"],locateDevices[key]["address"])
            for(var key1 in trackTools){

                if(trackTools[key1]["device"] === key){

                    romPaths.push(trackTools[key1]["romPath"])
                }
            }
            for(var key2 in trackers){

                if(trackers[key2]["device"] === key){

                    romPaths.push(trackers[key2]["romPath"])
                }
            }
            for(var key3 in connectPoints){

                if(connectPoints[key3]["device"] === key){

                    romPaths.push(connectPoints[key3]["romPath"])
                }
            }
            id_locator.initDevice(key,romPaths)
        }
    }
    function _loadDicomFile(path){

        id_view.filePath = path
        id_view._load()
    }
    function _getRegistMode(index){

        switch(index){

        case FinavConfigure.RegisterType.ManualRegistration:
            tipsInfo([qsTr("正在加载dicom图像..."),5000])
            id_fileDialog.open()
            break

        case FinavConfigure.RegisterType.AutoRegistration:
            break

        case FinavConfigure.RegisterType.CArmRegistration:
            break

        case FinavConfigure.RegisterType.Registration:

            tipsUpdate(1)
            id_registLoader.sourceComponent = id_registration
            id_registLoader.item.open()
            break
        default:
            break
        }
    }
    function _getMatrix(matrix){

        console.log(JSON.stringify(matrix))
    }
}
