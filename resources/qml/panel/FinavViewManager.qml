﻿import QtQuick 2.0
import com.vtk 1.0
import Fiui 1.0
import "../integration"
import "../dialog"
import "../"
/**@
     filename   FinavViewManager.qml
     brief      管理四个视图窗口
     version    1.0
     author     xh
     date       2021-01-22
     copyright  重庆博仕康科技有限公司
*/

Item{

    id: id_root
    /**
        @brief  视图窗口布局
    */
    property int layoutType: FinavConfigure.LayoutType.TwoToTwoType
    /**
        @brief  dicom加载路径
    */
    property string filePath
    /**
        @brief  视图之间的间距
    */
    property int offset: 4
    property real offsetW: (0.5-(offset/width)/2)
    property real offsetH: (0.5-(offset/height)/2)
    property real offsetThird: (offset/height)/3
    /**
        @brief  窗位
    */
    property real widLevel
    /**
        @brief  窗宽
    */
    property real widWidth
    /**
        @brief  阈值
    */
    property real threshold
    /**
        @brief  是否加载过图像
    */
    property bool isLoad: false
    NavWindow{

        id: id_vtk
        focus: true
        layoutType: 10
        anchors.fill: parent
        initialiseMap: {
        "stageMap":{
            "3d":{
                "viewType":"3d",
                "direction":"sagittal"
            },
            "2dsagittal":{
                "viewType":"2d",
                "direction":"sagittal"
            },
            "2dcoronal":{
                "viewType":"2d",
                "direction":"coronal"
            },
            "2daxial":{
                "viewType":"2d",
                "direction":"axial"
            }
          },
          "stageLayoutMap":{
                "2dsagittal":0,
                "2dcoronal":1,
                "3d":2,
                "2daxial":3
            }
        }
        Component.onCompleted: _TwoToTwoType()

    }
    Component.onCompleted: {

        id_vtk.errorMsg.connect(_errorMsg)
        id_vtk.viewEvent.connect(_viewEvent)
        id_root.layoutTypeChanged.connect(_modifyLayout)
        id_root.widLevelChanged.connect(_adjustWidLevel)
        id_root.widWidthChanged.connect(_adjustWidWidth)
        id_root.thresholdChanged.connect(_adjustThrehold)

    }
    function _testSTL(partNumber,stlPath){

        id_vtk.addActor("tool","tools",partNumber,{"path":stlPath})
        id_vtk.addActorToStage(partNumber,"2dsagittal")
        id_vtk.addActorToStage(partNumber,"3d")
        id_vtk.addActorToStage(partNumber,"2dcoronal")
        id_vtk.addActorToStage(partNumber,"2daxial")

    }
    function _getLocate(info){

        id_vtk.modifyActor("p_screwtip",{"position":info["location"]["position"]})
    }
    function _load(){

        tipsUpdate(0)
        id_vtk.removeAllActor()
        id_vtk.addActor("dicom3d","image","3d",{"path":filePath,"pickable":true})
        id_vtk.addActor("slice3d","image","sagittalSlice",{"dataSource":"3d","planePosition":[0,0,1,106,-1,0,0,50,0,-1,0,50,0,0,0,1],"pickable":true})
        id_vtk.addActor("slice3d","image","coronalSlice",{"dataSource":"3d","planePosition":[1,0,0,106,0,0,1,50,0,-1,0,50,0,0,0,1],"pickable":true})
        id_vtk.addActor("slice3d","image","axialSlice",{"dataSource":"3d","pickable":true})
        id_vtk.addActorToStage("3d","3d")
        id_vtk.addActorToStage("sagittalSlice","2dsagittal")
        id_vtk.addActorToStage("coronalSlice","2dcoronal")
        id_vtk.addActorToStage("axialSlice","2daxial")
        isLoad = true
    }
    function _adjustThrehold(){

        if(isLoad){

            id_vtk.modifyActor("3d",{"threshold": threshold})
        }
    }
    function _adjustWidLevel(){

        if(isLoad){

            id_vtk.modifyActor("sagittalSlice",{"colorLevel":widLevel})
            id_vtk.modifyActor("coronalSlice",{"colorLevel":widLevel})
            id_vtk.modifyActor("axialSlice",{"colorLevel":widLevel})
        }
    }
    function _adjustWidWidth(){

        if(isLoad){

            id_vtk.modifyActor("sagittalSlice",{"colorWindow":widWidth})
            id_vtk.modifyActor("coronalSlice",{"colorWindow":widWidth})
            id_vtk.modifyActor("axialSlice",{"colorWindow":widWidth})
        }
    }
    function _errorMsg(msg){

        tipsInfo([qsTr("dicom图像有误"),3000,"yellow"])
        console.log(msg)
    }
    function _viewEvent(info){

//        console.log(JSON.stringify(info))
        if(info["eventType"]==="pick"){

            var pos = _getXYZ(info["position"])
            id_vtk.modifyActor("sagittalSlice",{"planePosition":[0,0,1,pos[0],-1,0,0,pos[1],0,-1,0,pos[2],0,0,0,1]})
            id_vtk.modifyActor("coronalSlice",{"planePosition":[1,0,0,pos[0],0,0,1,pos[1],0,-1,0,pos[2],0,0,0,1]})
            id_vtk.modifyActor("axialSlice",{"planePosition":[1,0,0,pos[0],0,1,0,pos[1],0,0,1,pos[2],0,0,0,1]})
            return
        }
        id_vtk.resetStageCamera("3d")
        id_vtk.resetStageCamera("2dsagittal")
        id_vtk.resetStageCamera("2dcoronal")
        id_vtk.resetStageCamera("2daxial")
    }
    function _getXYZ(pos){

        var tmpXYZ = []
        tmpXYZ.push(pos[3],pos[7],pos[11])
        return tmpXYZ
    }

    function _modifyLayout(){

        switch(layoutType){

            case FinavConfigure.LayoutType.SingleType:
                _SingleType()
                break
            case FinavConfigure.LayoutType.LeftRightType:
               _LeftRightType()
                break
            case FinavConfigure.LayoutType.TopBottomType:
                _TopBottomType()
                break
            case FinavConfigure.LayoutType.OneToTwoType:
                _OneToTwoType()
                break
            case FinavConfigure.LayoutType.OneToThreeType:
                _OneToThreeType()
                break
            case FinavConfigure.LayoutType.TwoToTwoType:
                _TwoToTwoType()
                break
            default:
                break
        }
    }
    function _SingleType(){

        id_vtk.setLayoutValue([
                                 [0.0,0.0,0.0,0.0],
                                 [0.0,0.0,0.0,0.0],
                                 [0.0,0.0,1.0,1.0],
                                 [0.0,0.0,0.0,0.0]
                             ])
    }
    function _LeftRightType(){

        id_vtk.setLayoutValue([
                                 [0.0,0.0,offsetW,1.0],
                                 [0.0,0.0,0.0,0.0],
                                 [0.0,0.0,0.0,0.0],
                                 [1-offsetW,0.0,1.0,1.0]
                             ])
    }
    function _TopBottomType(){

        id_vtk.setLayoutValue([
                                 [0.0,1-offsetH,1.0,1.0],
                                 [0.0,0.0,0.0,0.0],
                                 [0.0,0.0,0.0,0.0],
                                 [0.0,0.0,1.0,offsetH]
                             ])
    }
    function _OneToTwoType(){

        id_vtk.setLayoutValue([
                                 [1-offsetW,1-offsetH,1.0,1.0],
                                 [0.0,0.0,0.0,0.0],
                                 [0.0,0.0,offsetW,1.0],
                                 [1-offsetW,0.0,1.0,offsetH]
                             ])
    }
    function _OneToThreeType(){

        id_vtk.setLayoutValue([
                                 [1-offsetW,0.67+offsetThird,1.0,1.0],
                                 [1-offsetW,0.33+offsetThird,1.0,0.67-offsetThird],
                                 [0.0,0.0,offsetW,1.0],
                                 [1-offsetW,0.0,1.0,0.33-offsetThird]
                             ])
    }
    function _TwoToTwoType(){

        id_vtk.setLayoutValue([
                                 [0.0,1-offsetH,offsetW,1.0],
                                 [1-offsetW,1-offsetH,1.0,1.0],
                                 [0.0,0.0,offsetW,offsetH],
                                 [1-offsetW,0.0,1.0,offsetH]
                             ])
    }
}
