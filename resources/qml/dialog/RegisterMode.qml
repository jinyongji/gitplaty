﻿import QtQuick 2.0
import QtQuick.Controls 2.5
import Qt.labs.platform 1.1
import Fiui 1.0
import ".."
/**@
     filename   RegisterMode.qml
     brief      选择注册方式界面
     version    1.0
     author     xh
     date       2021-04-01
     copyright  重庆博仕康科技有限公司
*/
FiDialog{

    /**
        @brief  注册方式
    */
    property int registerType: FinavConfigure.RegisterType.ManualRegistration
    /**
        @brief  文件选择路径
    */
    signal chooseRegisterType(int idx)
    id: id_root
    headerText: qsTr("选择注册方式")
    textSize: horSpacing(12)
    iconWidth: horSpacing(13)
    iconHeight: verSpacing(23)
    FiText{

        width: horSpacing(140)
        height: verSpacing(32)
        text: qsTr("选择注册方式如下:")
        anchors.top: parent.top
        anchors.topMargin: horSpacing(100)
        anchors.left: parent.left
        anchors.leftMargin: verSpacing(27)
        font.pixelSize: horSpacing(18)
    }
    Row{

        id: id_row
        anchors.top: parent.top
        anchors.topMargin: horSpacing(163)
        anchors.left: parent.left
        anchors.leftMargin: horSpacing(40)
        height: verSpacing(163)
        spacing: horSpacing(24)
        FiIconButton{

            id: id_manual
            width: horSpacing(163)
            height: horSpacing(163)
            checkable: true
            checked: true
            btnNormalSrc: "qrc:/Finav/image/manualregist_normal.svg"
            btnHoverSrc: "qrc:/Finav/image/manualregist_hover.svg"
            btnPressedSrc: "qrc:/Finav/image/manualregist_pressed.svg"
            btnCheckedSrc: "qrc:/Finav/image/manualregist_pressed.svg"
            btnText.text: qsTr("手动注册")
            btnText.font.pixelSize: horSpacing(16)
            btnText.anchors{

                top: id_manual.bottom
                horizontalCenter: id_manual.horizontalCenter
                topMargin: verSpacing(10)
            }
            onClicked: registerType = FinavConfigure.RegisterType.ManualRegistration
            Component.onCompleted: _updateState()
        }
        FiIconButton{

            id: id_auto
            width: horSpacing(163)
            height: horSpacing(163)
            checkable: true
            btnNormalSrc: "qrc:/Finav/image/manualregist_normal.svg"
            btnHoverSrc: "qrc:/Finav/image/manualregist_hover.svg"
            btnPressedSrc: "qrc:/Finav/image/manualregist_pressed.svg"
            btnCheckedSrc: "qrc:/Finav/image/manualregist_pressed.svg"
            btnText.text: qsTr("自动注册")
            btnText.font.pixelSize: horSpacing(16)
            btnText.anchors{

                top: id_auto.bottom
                horizontalCenter: id_auto.horizontalCenter
                topMargin: verSpacing(10)
            }
             onClicked: registerType = FinavConfigure.RegisterType.AutoRegistration
        }
        FiIconButton{

            id: id_carm
            width: horSpacing(163)
            height: horSpacing(163)
            checkable: true
            btnNormalSrc: "qrc:/Finav/image/manualregist_normal.svg"
            btnHoverSrc: "qrc:/Finav/image/manualregist_hover.svg"
            btnPressedSrc: "qrc:/Finav/image/manualregist_pressed.svg"
            btnCheckedSrc: "qrc:/Finav/image/manualregist_pressed.svg"
            btnText.text: qsTr("C臂追踪器")
            btnText.font.pixelSize: horSpacing(16)
            btnText.anchors{

                top: id_carm.bottom
                horizontalCenter: id_carm.horizontalCenter
                topMargin: verSpacing(10)
            }
             onClicked: registerType = FinavConfigure.RegisterType.CArmRegistration
        }
        FiIconButton{

            id: id_registrat
            width: horSpacing(163)
            height: horSpacing(163)
            checkable: true
            btnNormalSrc: "qrc:/Finav/image/manualregist_normal.svg"
            btnHoverSrc: "qrc:/Finav/image/manualregist_hover.svg"
            btnPressedSrc: "qrc:/Finav/image/manualregist_pressed.svg"
            btnCheckedSrc: "qrc:/Finav/image/manualregist_pressed.svg"
            btnText.text: qsTr("配准注册")
            btnText.font.pixelSize: horSpacing(16)
            btnText.anchors{

                top: id_registrat.bottom
                horizontalCenter: id_registrat.horizontalCenter
                topMargin: verSpacing(10)
            }
            onClicked: registerType = FinavConfigure.RegisterType.Registration
        }
    }
    ButtonGroup{

        id: id_buttonGroup
        buttons: id_row.children
    }

    FiIconButton{

        id: id_cancle
        width: horSpacing(119)
        height: verSpacing(34)
        btnNormalSrc: "qrc:/Finav/image/button_normal.svg"
        btnHoverSrc: "qrc:/Finav/image/button_hover.svg"
        btnPressedSrc: "qrc:/Finav/image/button_pressed.svg"
        btnText.text: qsTr("取消")
        btnText.font.pixelSize: horSpacing(16)
        anchors.right: parent.right
        anchors.rightMargin: horSpacing(60)
        anchors.bottom: parent.bottom
        anchors.bottomMargin: verSpacing(20)
        onClicked: id_modeLoader.sourceComponent = undefined
    }
    FiIconButton{

        id: id_confirm
        width: horSpacing(119)
        height: verSpacing(34)
        btnNormalSrc: "qrc:/Finav/image/button_normal.svg"
        btnHoverSrc: "qrc:/Finav/image/button_hover.svg"
        btnPressedSrc: "qrc:/Finav/image/button_pressed.svg"
        btnText.text: qsTr("确定")
        btnText.font.pixelSize: horSpacing(16)
        anchors.right: id_cancle.left
        anchors.rightMargin: horSpacing(28)
        anchors.verticalCenter: id_cancle.verticalCenter
        onClicked: _chooseRegisterType()
    }
    function _chooseRegisterType(){

        switch(registerType){

        case FinavConfigure.RegisterType.ManualRegistration:
            chooseRegisterType(FinavConfigure.RegisterType.ManualRegistration)
            break

        case FinavConfigure.RegisterType.AutoRegistration:
            chooseRegisterType(FinavConfigure.RegisterType.AutoRegistration)
            break

        case FinavConfigure.RegisterType.CArmRegistration:
            chooseRegisterType(FinavConfigure.RegisterType.CArmRegistration)
            break

        case FinavConfigure.RegisterType.Registration:
            chooseRegisterType(FinavConfigure.RegisterType.Registration)
            break

        default:
            break
        }
        id_modeLoader.sourceComponent = undefined
    }
}


