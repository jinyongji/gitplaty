﻿import QtQuick 2.0
import QtQuick.Window 2.12
import QtQuick.Controls 2.5
import Fiui 1.0
/**@
     filename   About.qml
     brief      关于界面
     version    1.0
     author     xh
     date       2021-01-28
     copyright  重庆博仕康科技有限公司
*/
FiDialog{

    id: id_root
    closePolicy: Popup.CloseOnPressOutside
    modal: false
    background:Image{

        source: "qrc:/Finav/image/about.svg"
        sourceSize.width: parent.width
        sourceSize.height: parent.height
        FiIconButton{

            width: horSpacing(104)
            height: verSpacing(30)
            btnNormalSrc: "qrc:/Finav/image/button_normal.svg"
            btnHoverSrc: "qrc:/Finav/image/button_hover.svg"
            btnPressedSrc: "qrc:/Finav/image/button_pressed.svg"
            anchors.right: parent.right
            anchors.rightMargin: horSpacing(60)
            anchors.bottom: parent.bottom
            anchors.bottomMargin: verSpacing(20)
            FiText{

                text: qsTr("退 出")
                anchors.centerIn: parent
                font.pixelSize: horSpacing(12)
            }
            onClicked: id_root.close()
        }
    }
}
