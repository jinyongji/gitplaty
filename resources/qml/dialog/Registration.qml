import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Window 2.0
import com.poseCalculator 1.0
import com.vtk 1.0
import Fiui 1.0
import "../integration"

/**@
     filename   Registration.qml
     brief      配准注册界面
     version    1.0
     author     xh
     date       2021-04-17
     copyright  重庆博仕康科技有限公司
*/
Popup{

    id: id_root
    property string pathOf3DImage:"../dicomImage/model/spine4"
    property string pathOfIntraCoronal:"../dicomImage/IMG00002"
    property string pathOfIntraSagittal:"../dicomImage/IMG00003"
//    property string pathOf3DImage:"/home/worker/WorkSpace/bosscome/fiui2/dicomImage/wholeModel"
//    property string pathOfIntraCoronal:"/home/worker/WorkSpace/bosscome/fiui2/dicomImage/NailDCM/DCM3/IMG00003"
//    property string pathOfIntraSagittal:"/home/worker/WorkSpace/bosscome/fiui2/dicomImage/NailDCM/DCM3/IMG00001"

    property alias idPosCalculator: id_posCalculator
//    property alias g_idChoose : id_choose
    property real index: 0

    property bool isProjected:false
    property real  intra2DLastRotatedAngle: 0
    property int circleCenterDetectFinishedCount :0
    //控制3D图像只padding一次
    property int is3DIn3DViewPadded:0
    property int is3DInSagittalPadded:0
    property int is3DInCoroanlPadded:0

    //窗宽床位调整
    signal changeProjected2DWindowLevel(var info)
    signal changeProjected2DWindowWidth(var info)
    signal changeIntra2DWindowLevel(var info)
    signal changeIntra2DWindowWidth(var info)
    signal change3DImageThreshold(var info)

    //choose point
    property bool boolChooseSagittalPeak:false
    property bool boolChooseCoronalPeak:false
    property bool boolChooseRGPInIntraSagittal:false
    property bool boolChooseRGPInIntraCoronal:false
    property bool boolChooseRGPInProjectedSagittal:false
    property bool boolChooseRGPInProjectedCoronal:false

    property bool isIntraCoronalPeakPassed: false
    property bool isIntraSagittalPeakPassed: false
    property bool isAdjustIntraImagesFinished:false //whether has been done

    property var oldImage3DOrigin:[0.0,0.0,0.0]

    property int showWhichGroupBotton:0
    signal sendResult(var matrix)
    x: Screen.width/2 - width/2
    y: Screen.height/2 - height/2
    closePolicy: Popup.NoAutoClose
    modal: true
    NavWindow{
        id:id_NavWindow
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.margins: 10
        width: horSpacing(864)
        height: verSpacing(578)

        initialiseMap:{
            "layoutType":1,

            "stageMap":{
                "id_view3D":{
                    "viewType":"3d",
                    "direction":"axial"
                },
                "id_view2DSagittal":{
                    "viewType":"2d",
                    "direction":"sagittal"
                },
                "id_view2DCoronal":{
                    "viewType":"2d",
                    "direction":"coronal"
                }

            },

            "stageLayoutMap":{
                "id_view3D":1,
                "id_view2DSagittal":0,
                "id_view2DCoronal":2,
            },
            "actorMap":{
                "image":{

                    "preOprate3DActor":{
                        "type":"dicom3d",
                        "param":
                        {
                            "path":pathOf3DImage,
                            "threshold":-800,
                            "pickable":false,
                            "originChange":1
                            }
                    },


                    "intraSagittalActor":{
                        "type":"dicom2d",
                        "param":{
                            "path":pathOfIntraSagittal,
                            "position":id_posCalculator.getInitSagittalMatrix(),
                            "colorLevel":2048,
                            "pickable":false,
                            "colorWindow":4095,}
                    },

                    "intraCoronalActor":{
                        "type":"dicom2d",
                        "param":{
                            "path":pathOfIntraCoronal,
                            "position":id_posCalculator.getInitCoronalMatrix(),
                            "colorLevel":2048,
                            "pickable":false,
                            "colorWindow":4095,}
                    },

                    "axies":
                    {
                        "type":"axis",
                        "param":{
                            pickable:false,
                            length:[100,100,100]
                        }
                    }

                }

            },
            "actorStageMap":{

                "preOprate3DActor":[
                    "id_view3D"
                ],

                "intraSagittalActor":[
                    "id_view2DSagittal",
                    "id_view3D"
                ],

                "intraCoronalActor":[
                    "id_view2DCoronal",
                    "id_view3D"
                ],

                "axies":[
                    "id_view2DCoronal",
                    "id_view2DSagittal",
                    "id_view3D"
                ]

            }
        }

    }

    PoseCalculator{
      id:id_posCalculator
    }

    ChoosePoints{

        id: id_choose
        visible: true
        width: 200
        height: 400
        anchors.left: id_NavWindow.right
        anchors.leftMargin: 20
    }

    AdjustImage{

        id: id_adjust
        visible: false
        width: 200
        height: 400
        anchors.left: id_NavWindow.right
        anchors.leftMargin: 20
    }



    Text {
        id: id_image3DThresholdText
        text: qsTr("3D Threshold")
        anchors.left: id_NavWindow.left
        anchors.top: id_NavWindow.bottom
        anchors.leftMargin: 25
        anchors.topMargin: 15
    }


    FiSlider{

        id: id_image3DThresholdSlider
        width: 950
        height: 5
        sliderWidSize: 20
        sliderHeiSize: 20
        minValue: -1024
        maxValue: 3071
        sliderValue:0
        sliderStyleSrc: "qrc:/Finav/image/sliderbar.svg"
        anchors.left: id_image3DThresholdText.left
        anchors.top: id_image3DThresholdText.bottom
        anchors.topMargin: 5

        onSliderValueChanged:
        {
//           console.log("image 3D Threshold slider changed",id_image3DThresholdSlider.sliderValue)
           change3DImageThreshold(id_image3DThresholdSlider.sliderValue)
        }
    }



    Text {
        id: id_intra2DWLText
        text: qsTr("intra WindowLevel")
        anchors.left: id_image3DThresholdSlider.left
        anchors.top: id_image3DThresholdSlider.bottom
        anchors.topMargin: 15
    }

    FiSlider{ //术中2D窗位

        id: id_intra2DWindowLevel
        width: 450
        height: 5
        sliderWidSize: 20
        sliderHeiSize: 20
        minValue: -1024
        maxValue: 3071
        sliderValue:0
        sliderStyleSrc: "qrc:/Finav/image/sliderbar.svg"
        anchors.left: id_intra2DWLText.left
        anchors.top: id_intra2DWLText.bottom
        anchors.topMargin: 5

        onSliderValueChanged:
        {
           changeIntra2DWindowLevel(id_intra2DWindowLevel.sliderValue)
        }
    }

    Text {
        id: id_intra2DWWText
        text: qsTr("intra WindowWidth")
        anchors.left: id_intra2DWindowLevel.left
        anchors.top: id_intra2DWindowLevel.bottom
        anchors.topMargin: 15
    }

    FiSlider{//术中2Ds窗宽

        id: id_intra2DWindowWidth
        width: 450
        height: 5
        sliderWidSize: 20
        sliderHeiSize: 20
        minValue: 0
        maxValue: 4096
        sliderValue:0
        sliderStyleSrc: "qrc:/Finav/image/sliderbar.svg"
        anchors.left: id_intra2DWWText.left
        anchors.top: id_intra2DWWText.bottom
        anchors.topMargin: 5

        onSliderValueChanged:
        {
           changeIntra2DWindowWidth(id_intra2DWindowWidth.sliderValue)
        }
    }


    Text {
        id: id_projected2DWLText
        text: qsTr("projected WindowLevel")
        anchors.left: id_intra2DWindowLevel.right
        anchors.top: id_intra2DWLText.top
        anchors.leftMargin: 15
    }



    FiSlider{ //投影2D窗位

        id: id_projectedWindowLevel
        width: 450
        height: 5
        sliderWidSize: 20
        sliderHeiSize: 20
        minValue: -1024
        maxValue: 3071
        sliderValue:0
        sliderStyleSrc: "qrc:/Finav/image/sliderbar.svg"
        anchors.left: id_intra2DWindowLevel.right
        anchors.top: id_projected2DWLText.bottom
        anchors.leftMargin : 15
        anchors.topMargin : 5

        onSliderValueChanged:
        {
            if(isProjected)
            {
                changeProjected2DWindowLevel(id_projectedWindowLevel.sliderValue)
            }

        }
    }

    Text {
        id: id_projected2DWWText
        text: qsTr("projected WindowWidth")
        anchors.left: id_intra2DWindowWidth.right
        anchors.top: id_intra2DWWText.top
        anchors.leftMargin: 15
    }


    FiSlider{//投影2Ds窗宽

        id: id_projectedWindowWidth
        width: 450
        height: 5
        sliderWidSize: 20
        sliderHeiSize: 20
        minValue: 0
        maxValue: 4096
        sliderValue:0
        sliderStyleSrc: "qrc:/Finav/image/sliderbar.svg"
        anchors.left: id_projected2DWWText.left
        anchors.top: id_projected2DWWText.bottom
        anchors.topMargin: 5

        onSliderValueChanged:
        {
            if(isProjected)
            {
                changeProjected2DWindowWidth(id_projectedWindowWidth.sliderValue)
            }
        }
    }


    FiButton{

        id: id_nextStep
        width: 100
        height: 50
        anchors.top: id_NavWindow.bottom
        anchors.topMargin: 20
        anchors.right: id_NavWindow.right
        text:"下一步"
        onClicked: nextstep()
    }
    FiButton{

        id: id_preStep
        width: 100
        height: 50
        anchors.top: id_NavWindow.bottom
        anchors.topMargin: 20
        anchors.right: id_nextStep.left
        anchors.rightMargin: 20
        text:"上一步"
        onClicked: preStep()
    }
    FiButton{

        id: id_Quit
        width: 100
        height: 50
        anchors.top: id_NavWindow.bottom
        anchors.topMargin: 20
        anchors.left: id_nextStep.right
        anchors.leftMargin: 20
        text:"退出"
        onClicked: {

            id_registLoader.sourceComponent = undefined
            tipsUpdate(0)
        }
    }



    ////////////////////////
    ///////////////////// function

    function setAllPickStatusOff()
    {
        setAllActorPickStatusOff()
        setAllPickEventStatusOff()

    }
    function setAllActorPickStatusOff()
    {
        turnActorPickableStatusOff("preOprate3DActor")
        turnActorPickableStatusOff("intraCoronalActor")
        turnActorPickableStatusOff("intraSagittalActor")
        turnActorPickableStatusOff("projectedCoronal")
        turnActorPickableStatusOff("projectedSagittal")
    }


    function setAllPickEventStatusOff()
    {
        id_root.boolChooseSagittalPeak=false
        id_root.boolChooseCoronalPeak=false
        id_root.boolChooseRGPInIntraSagittal=false
        id_root.boolChooseRGPInIntraCoronal=false
        id_root.boolChooseRGPInProjectedSagittal=false
        id_root.boolChooseRGPInProjectedCoronal=false
    }


    function turnActorPickableStatusOn(actorId)
    {
        var changeActorPickStatus =  {
                pickable:true
                }
        id_NavWindow.modifyActor(actorId,changeActorPickStatus)
    }


    function turnActorPickableStatusOff(actorId)
    {
        var changeActorPickStatus =  {
                pickable:false
                }
        id_NavWindow.modifyActor(actorId,changeActorPickStatus)
    }

    //产生术中投影
     function doProject()
     {
         var tempProjectedCoronal =
                     {
//                         path:pathOf3DImage,
                         dataSource:"preOprate3DActor",
                         pickable:false,
                         position:id_posCalculator.getInitProjectedCoronalMatrix(),
                         plane:1, //正位片
                         originPosition:1,
                         colorLevel:219,
                         colorWindow:793,
                         method:0,
                         opacityEffect:true
                     }
        id_NavWindow.addActor("project3d","image","projectedCoronal", tempProjectedCoronal)
        id_NavWindow.addActorToStage("projectedCoronal","id_view2DCoronal")


         var tempProjectedSagittal =
                     {
                         dataSource:"preOprate3DActor",
                         position:id_posCalculator.getInitProjectedSagittalMatrix(),
                         pickable:false,
                         plane:0, //侧位片
                         originPosition:1,
                         colorLevel:219,
                         colorWindow:793,
                         method:0,
                         opacityEffect:true,
                     }
         id_NavWindow.addActor("project3d","image","projectedSagittal", tempProjectedSagittal)
         id_NavWindow.addActorToStage("projectedSagittal","id_view2DSagittal")

         isProjected = true;
     }

    function nextstep()
    {
        if(showWhichGroupBotton==2){
            return
        }

        showWhichGroupBotton++
        switch(showWhichGroupBotton )
        {
        case 0:
            id_choose.visible = true
            id_adjust.visible = false
            break
        case 1:
            id_choose.visible = false
            id_adjust.visible = true
            doProject()
            break
        case 2:
            id_choose.visible = false
            id_adjust.visible = false
            break
        default:
            break
        }

    }

    function preStep()
    {
        if(showWhichGroupBotton==0)
        {
            return
        }

        showWhichGroupBotton--
        switch(showWhichGroupBotton )
        {
        case 0:
            id_choose.visible = true
            id_adjust.visible = false
            break
        case 1:
            id_choose.visible = false
            id_adjust.visible = true
            break
        case 2:
            id_choose.visible = false
            id_adjust.visible = false
            break
        }
    }

    function abstracXYZfromMatrix(positon)
    {
        var tempXYZ =[]
        tempXYZ.push(positon[3],positon[7],positon[11])
        return tempXYZ
    }

    function checkToBeginCalculateUseInteractData()
    {
        if(circleCenterDetectFinishedCount ==2 )
        {
            id_posCalculator.calculateUseInteractData()
            circleCenterDetectFinishedCount=0
            updataRespacingResult()
        }
    }

    //  获取更新像素间距后的结果
    function updataRespacingResult()
    {
        //更新术中正位片位置 （改origin）
        var updatedCoronalMatrix = id_posCalculator.getUpdatedCoronalMatrix()
        var changeCoronalOrgin =
        {
            actionType:"setImageOrigin2d",
            origin:[0,-updatedCoronalMatrix[11]] //默认orgin为0，后续应改为在原origin基础上移动
        }
        id_NavWindow.modifyActorData("intraCoronalActor",changeCoronalOrgin)

        //更新术中正侧位片像素间距
        var newCoronalSpacing = id_posCalculator.getNewSpacingOfCoronal()
        var newSagittalSpacing = id_posCalculator.getNewSpacingOfSagittal()

        var updataIntraCoronalspacing=
        {
            actionType:"setImageSpacing2d",
            spacing:[newCoronalSpacing,newCoronalSpacing] //同一2D图像上 xy方向像素间距应相等
        }
        console.log("updataIntraCoronalspacing",JSON.stringify(updataIntraCoronalspacing))
        id_NavWindow.modifyActorData("intraCoronalActor",updataIntraCoronalspacing)

        var updataIntraSagittalspacing=
        {
            actionType:"setImageSpacing2d",
            spacing:[newSagittalSpacing,newSagittalSpacing] //同一2D图像上 xy方向像素间距应相等
        }
        id_NavWindow.modifyActorData("intraSagittalActor",updataIntraSagittalspacing)

        //获取重新设置像素间距后  3D追踪钉
        var  cylinderOfNail =
        {
            pickable:false,
            isByPoints:true,
            point1:abstracXYZfromMatrix(id_posCalculator.getRespacingNailCenterIn3D()),
            point2:abstracXYZfromMatrix(id_posCalculator.getRespacingNailPeakIn3D()),
            color:[1,0,0,1],
            radius:5
        }
        console.log("cylinderOfNail ",JSON.stringify(cylinderOfNail))
        id_NavWindow.addActor("cylinder","tools","cylinderOfNail",cylinderOfNail)
        id_NavWindow.addActorToStage("cylinderOfNail","id_view3D")
    }



    function removeIntraImageActor()
    {
        id_NavWindow.removeActor("intraSagittalActor")
        id_NavWindow.removeActor("intraCoronalActor")
    }

    function removeProjectedImageActor()
    {
        id_NavWindow.removeActor("projectedCoronal")
        id_NavWindow.removeActor("projectedSagittal")
    }

    function removeRGPS()
    {
        id_NavWindow.removeActor("RGPInIProjectedCoronal")
        id_NavWindow.removeActor("RGPInIProjectedSagittal")
        id_NavWindow.removeActor("RGPInIntraSagittal")
        id_NavWindow.removeActor("RGPInIntraCoronal")
    }

    function removeAssistantTools()
    {
        removeIntraImageActor()
        removeProjectedImageActor()
        removeRGPS()
        id_NavWindow.removeActor("axies")
    }

    ///////////////
    ////////// Connections
    ///////////////

    //target: id_root
    Connections{

        target: id_root


        onChangeProjected2DWindowLevel:function changeWindowLevel(windowLevel)
        {
            var changeWindowLevel =
                    {
                        colorLevel:windowLevel
                     }
            id_NavWindow.modifyActor("projectedCoronal",changeWindowLevel)
            id_NavWindow.modifyActor("projectedSagittal",changeWindowLevel)
        }

        onChangeProjected2DWindowWidth:function changeWindowWidth(windowWidth)
        {
            var changeWindowWidth =
                    {
                        colorWindow:windowWidth
                     }
            id_NavWindow.modifyActor("projectedCoronal",changeWindowWidth)
            id_NavWindow.modifyActor("projectedSagittal",changeWindowWidth)
        }


        onChangeIntra2DWindowLevel:function intra2DWindowLevel(windowLevel)
        {
            var changeWindowLevel =
                    {
                        colorLevel:windowLevel
                     }
            id_NavWindow.modifyActor("intraSagittalActor",changeWindowLevel)
            id_NavWindow.modifyActor("intraCoronalActor",changeWindowLevel)
        }

        onChangeIntra2DWindowWidth:function intra2DWindowWidth(windowWidth)
        {
            var changeWindowWidth =
                    {
                        colorWindow:windowWidth
                     }
            id_NavWindow.modifyActor("intraSagittalActor",changeWindowWidth)
            id_NavWindow.modifyActor("intraCoronalActor",changeWindowWidth)
        }

        onChange3DImageThreshold: function change3DImageThreshold(info)
        {
            var tempChnage3DThreshod =
                    {
                        threshold:info
                     }
              id_NavWindow.modifyActor("preOprate3DActor",tempChnage3DThreshod)
        }








    }


    Connections
       {
           target: id_NavWindow

           //render window
           onViewEvent:function renderView(info)
           {

               if(info["eventType"] === "actorStageChange" )
               {
                   if(info["actorId"]==="preOprate3DActor")
                   {
                       oldImage3DOrigin = id_NavWindow.getActorParams("preOprate3DActor")["imageOrigin"]
                       console.log("oldImage3DOrigin",JSON.stringify(oldImage3DOrigin))
                       id_NavWindow.resetStageCamera("id_view3D")
                   }
                   else if(info["actorId"]==="intraSagittalActor")
                   {
                       id_NavWindow.resetStageCamera("id_view2DSagittal")
                   }
                   else if(info["actorId"]==="intraCoronalActor")
                   {
                       id_NavWindow.resetStageCamera("id_view2DCoronal")
                   }

               }

               else if( info["eventType"]==="pick")
               {
                   if(boolChooseSagittalPeak&&!isIntraSagittalPeakPassed)
                   {
                       var tempSagittalXYZ =abstracXYZfromMatrix(info["position"])
                       console.log("tempSagittalXYZ",tempSagittalXYZ)
                       id_posCalculator.setSagittalPeakPoints(tempSagittalXYZ)
                       id_choose.peakInSagittal=false
                       var sphereSagittal =
                                   {
                                       radius:20,
                                       position:info["position"]
                                   }
                        id_NavWindow.addActor("sphere","accessory","sphereSagittalPeak",sphereSagittal)
                        id_NavWindow.addActorToStage("sphereSagittalPeak","id_view2DSagittal")
                        setAllPickStatusOff()
                        isIntraSagittalPeakPassed = true
                   }
                   else if(boolChooseCoronalPeak&&!isIntraCoronalPeakPassed)
                   {
                      var tempCoronalXYZ = abstracXYZfromMatrix(info["position"])
                       console.log("tempCoronalXYZ",tempCoronalXYZ)
                      id_posCalculator.setCoronalPeakPoints(tempCoronalXYZ)
                      id_choose.peakInCoronal=false
                      var sphereCoronal =
                              {
                                  radius:20,
                                  position:info["position"]
                              }
                      id_NavWindow.addActor("sphere","accessory","sphereCoronalPeak",sphereCoronal)
                      id_NavWindow.addActorToStage("sphereCoronalPeak","id_view2DCoronal")
                      setAllPickStatusOff()
                      isIntraCoronalPeakPassed = true
                   }

                   // 粗配 术中正位片选点
                   else if(id_root.boolChooseRGPInIntraCoronal)
                   {
                       var tempSagittalXYZ =abstracXYZfromMatrix(info["position"])
                       id_posCalculator.setRGPInIntraCoronal(tempSagittalXYZ)
                       id_root.boolChooseRGPInIntraCoronal=false
                       var sphereActor =
                                   {
                                       radius:5,
                                       position:info["position"],
                                       color:[0.1,0.4,0.6,1]
                                   }
                       id_NavWindow.addActor("sphere","accessory","RGPInIntraCoronal",sphereActor)
                       id_NavWindow.addActorToStage("RGPInIntraCoronal","id_view2DCoronal")
                       setAllPickStatusOff()
                   }

                   // 粗配 投影正位片选点
                   else if(id_root.boolChooseRGPInProjectedCoronal)
                   {
                       console.log(JSON.stringify(info["position"]))
                       var tempSagittalXYZ =abstracXYZfromMatrix(info["position"])
                       id_posCalculator.setRGPInProjectedCoronal(tempSagittalXYZ)
                       id_root.boolChooseRGPInProjectedCoronal=false
                       var sphereActor =
                                   {
                                       radius:5,
                                       position:info["position"],
                                       color:[0.1,0.4,0.6,1]
                                   }
                       id_NavWindow.addActor("sphere","accessory","RGPInIProjectedCoronal",sphereActor)
                       id_NavWindow.addActorToStage("RGPInIProjectedCoronal","id_view2DCoronal")
                       setAllPickStatusOff()
                   }

                   // 粗配 术中侧位片选点
                    else if(id_root.boolChooseRGPInIntraSagittal)
                   {
                       var tempSagittalXYZ =abstracXYZfromMatrix(info["position"])
                       id_posCalculator.setRGPInIntraSagittal(tempSagittalXYZ)
                       id_root.boolChooseRGPInIntraSagittal=false
                       var sphereActor =
                                   {
                                       radius:5,
                                       position:info["position"],
                                       color:[0.1,0.4,0.6,1]
                                   }
                       id_NavWindow.addActor("sphere","accessory","RGPInIntraSagittal",sphereActor)
                       id_NavWindow.addActorToStage("RGPInIntraSagittal","id_view2DSagittal")
                       setAllPickStatusOff()
                   }

                   // 粗配 投影侧位片选点
                    else if(id_root.boolChooseRGPInProjectedSagittal)
                   {
                       var tempSagittalXYZ =abstracXYZfromMatrix(info["position"])
                       id_posCalculator.setRGPInProjectedSagittal(tempSagittalXYZ)
                       id_root.boolChooseRGPInProjectedSagittal=false
                       var sphereActor =
                                   {
                                       radius:5,
                                       position:info["position"],
                                       color:[0.1,0.4,0.6,1]
                                   }
                       id_NavWindow.addActor("sphere","accessory","RGPInIProjectedSagittal",sphereActor)
                       id_NavWindow.addActorToStage("RGPInIProjectedSagittal","id_view2DSagittal")
                       setAllPickStatusOff()
                   }

               }


               else if(info["eventType"]==="actionFinished")
               {
                   console.log("actionFinished",JSON.stringify(info))
                   if(info["actorId"]==="intraSagittalActor")
                   {
                        if(info["actionType"] ==="opencvHoughCirclesDetector" )
                        {
                          id_posCalculator.setSagittalBallCenter(info["detectResult"][0]["x"],info["detectResult"][0]["y"])
                          id_posCalculator.setSagittalBallRadius(info["detectResult"][0]["radius"])
                          circleCenterDetectFinishedCount+=1
                          checkToBeginCalculateUseInteractData()
                        }
                        else if(info["actionType"] ==="setImageSpacing2d")
                        {
                            id_NavWindow.removeActor("sphereSagittalPeak")
                            id_NavWindow.resetStageCamera("id_view2DSagittal");
                            id_NavWindow.resetStageCamera("id_view3D");
                        }
                   }




                   else if(info["actorId"]==="intraCoronalActor")
                   {
                       if(info["actionType"] ==="opencvHoughCirclesDetector" )
                       {
                           id_posCalculator.setCoronalBallCenter(info["detectResult"][0]["x"],info["detectResult"][0]["y"])
                           id_posCalculator.setCoronalBallRadius(info["detectResult"][0]["radius"])
                           circleCenterDetectFinishedCount+=1
                           checkToBeginCalculateUseInteractData()
                       }
                       else if(info["actionType"] ==="setImageSpacing2d")
                       {
                           console.log("reset camera22222222222333333333333")
                            id_NavWindow.removeActor("sphereCoronalPeak")
                            id_NavWindow.resetStageCamera("id_view2DCoronal");
                            id_NavWindow.resetStageCamera("id_view3D");
                       }
                   }
              }


            }
       }



//        onSendCoronalItemModified:function pad3DImage(changedType,changedId,changedMap)
//        {
//                     console.log("99999999999999999999")
////            if(is3DInCoroanlPadded ==0 && changedType === "project3d"&& changedId==="projectedCoronal")
////            {
////                var pad3DImage=
////                        {

////                             actionType:"padImageUseConstant3d",
////                             lowerExtendRegion:[0,0,20],
////                             upperExtendRegion:[0,0,20],
////                             constant:-1024
////                        }
////                console.log("-----444444444444444444444------------")
////                id_view.view2DCoronal.modifyDataItem("project3d","projei8hjyuuuuuuuuuuuunCoroanlPadded=1
////            }
//        }

//        onSendSagittalItemModified:function pad3DImage(changedType,changedId,changedMap)
//        {
//              console.log("500500500")
//             console.log(JSON.stringify(id_view.view2DSagittal.model))
//            if(is3DInSagittalPadded ==0 && changedType === "project3d"&& changedId==="projectedSagittal")
//            {
//                var pad3DImage=
//                        {

//                             actionType:"padImageUseConstant3d",
//                             lowerExtendRegion:[0,0,60],
//                             upperExtendRegion:[0,0,0],
//                             constant:-1024
//                        }
//                console.log("-----555555555555------------")
//                id_view.view2DSagittal.modifyDataItem("project3d","projectedSagittal",pad3DImage)
////                 id_view.view3D.modifyDataItem("dicom3d","preOprate3DActor",pad3DImage)
//                is3DInSagittalPadded=1
//            }
//        }


//    }



    //target: id_choose
    Connections
    {
        target: id_choose

        onBeginAdjustPositon:
        {
            if(isIntraCoronalPeakPassed&&isIntraSagittalPeakPassed)
            {
                //chuanru spacing
                var initCoronalSpacing  = id_NavWindow.getActorParams("intraCoronalActor")["imageSpacing"]
                var initCoronalOrigin  = id_NavWindow.getActorParams("intraCoronalActor")["imageOrigin"]
                console.log("initCoronalSpacing",initCoronalSpacing[0])
                id_posCalculator.setInitCoronalSpacing(initCoronalSpacing[0],initCoronalSpacing[1])
                id_posCalculator.setInitCoronalOrigin(initCoronalOrigin[0],initCoronalOrigin[1])

                var initSagittalSpacing  = id_NavWindow.getActorParams("intraSagittalActor")["imageSpacing"]
                var initSagittalOrigin  = id_NavWindow.getActorParams("intraSagittalActor")["imageOrigin"]
                id_posCalculator.setInitSagittalSpacing(initSagittalSpacing[0],initSagittalSpacing[1])
                id_posCalculator.setInitSagittalOrigin(initSagittalOrigin[0],initSagittalOrigin[1])



                console.log("onBeginAdjustPositon active")
                var houghCircleDetectorCoronal = {
                    actionType:"opencvHoughCirclesDetector",
                    minDist:100,
                    minRadius:10,
                    maxRadius:80,
                    param1:80,
                    param2:27
                }
                id_NavWindow.modifyActorData("intraCoronalActor",houghCircleDetectorCoronal)

                console.log("opencvHoughCirclesDetector in sagittal active")
                var houghCircleDetectorSagittal = {
                    actionType:"opencvHoughCirclesDetector",
                    minDist:100,
                    minRadius:20,
                    maxRadius:90,
                    param1:80,
                    param2:27
                }
                id_NavWindow.modifyActorData("intraSagittalActor",houghCircleDetectorSagittal)
            }
            else
            {
                console.log("pick nail peaks first!")
            }
        }

        onRotateAngle:function rotateIntra2D(angleInDegree)
        {
            console.log("rotate intra 2D")
            var newRotateAngle = (angleInDegree-intra2DLastRotatedAngle)
            intra2DLastRotatedAngle = angleInDegree
            //术中正位片旋转
            var oldIntraCoronalSpacing = id_NavWindow.getActorParams("intraCoronalActor")["imageSpacing"]
            var oldIntraCoronalSize= id_NavWindow.getActorParams("intraCoronalActor")["imageSize"]

            var tempRotateIntra2DCoronal = {
                        actionType:"resample2DImage",
                        angleInDegree:newRotateAngle,
                        newSpacing:oldIntraCoronalSpacing,
                        newSize:oldIntraCoronalSize
                        }

              id_NavWindow.modifyActorData("intraCoronalActor",tempRotateIntra2DCoronal)

              //术中侧位片旋转
             var oldIntraSagittalSpacing = id_NavWindow.getActorParams("intraSagittalActor")["imageSpacing"]
             var oldIntraSagittalSize= id_NavWindow.getActorParams("intraSagittalActor")["imageSize"]

             var tempRotateIntra2DSagittal= {
                        actionType:"resample2DImage",
                        angleInDegree:newRotateAngle,
                        newSpacing:oldIntraSagittalSpacing,
                        newSize:oldIntraSagittalSize
                        }

              id_NavWindow.modifyActorData("intraSagittalActor",tempRotateIntra2DSagittal)

        }

        onChangePickEventStatus: function changeStatus(info)
        {
            if(info === "peakInSagittal")
            {
                setAllPickStatusOff()
                id_root.boolChooseSagittalPeak =true
                turnActorPickableStatusOn("intraSagittalActor")
            }
            else if(info === "peakInCoronal")
            {
                setAllPickStatusOff()
                id_root.boolChooseCoronalPeak=true
                turnActorPickableStatusOn("intraCoronalActor")
            }
            else
            {
                console.log("onChangePickEventStatus error")
            }

        }


    }


// target:id_adjust
    Connections
    {
    target:id_adjust

    onAdjust3DImage:function adjustimage3D(opration)
    {
        var tempx =0
        var tempy =0
        var tempz =0

        var rotateAxis ="AXIS_X"
        var rotateAngle =0

//        console.log(opration)

        if(opration === "x+1"||opration === "y+1"||opration === "z+1"||
            opration === "x-1"|| opration === "y-1"|| opration === "z-1")
        {
            if(opration === "x+1")
            {
                tempx +=1
            }
            else if(opration === "y+1")
            {
                tempy +=1
            }
            else if(opration === "z+1")
            {
                tempz +=1
            }
            else if(opration === "x-1")
            {
                tempx -=1
            }
            else if(opration === "y-1")
            {
                tempy -=1
            }
            else if(opration === "z-1")
            {
                tempz -=1
            }


//                var tempTranslation3DActor= {
//                    actionType:"imageOriginTranslation3d",
//                    translation:[tempx,tempy,tempz]
//                    }
//                id_NavWindow.modifyActor("preOprate3DActor",tempTranslation3DActor)

            var tempTranslationForProjection= {
                actionType:"imageOriginTranslation3d",
                translation:[tempx,tempy,tempz]
                }

            id_NavWindow.modifyActorData("projectedCoronal",tempTranslationForProjection)
//                id_NavWindow.modifyActorData("projectedSagittal",tempTranslationForProjection)
            console.log(tempx," ",tempy,"  ",tempz)
        }
        else if(opration === "rotate along x postive"||
                    opration === "rotate along y positive"||
                    opration === "rotate along z positive"||
                    opration === "rotate along x negative"||
                    opration === "rotate along y negative"||
                    opration === "rotate along z nagative")
            {
                        if(opration === "rotate along x postive")
                        {
                            rotateAxis ="AXIS_X"
                            rotateAngle = 5
                        }
                        else if(opration === "rotate along y positive")
                        {
                            rotateAxis ="AXIS_Y"
                            rotateAngle  = 5
                        }
                        else if(opration === "rotate along z positive")
                        {
                            rotateAxis ="AXIS_Z"
                            rotateAngle = 5
                        }
                                else if(opration === "rotate along x negative")
                        {
                            rotateAxis ="AXIS_X"
                            rotateAngle = -4
                        }
                                else if(opration === "rotate along y negative")
                        {
                            rotateAxis ="AXIS_Y"
                            rotateAngle = -4
                        }
                                else if(opration === "rotate along z nagative")
                        {
                            rotateAxis ="AXIS_Z"
                            rotateAngle = -4
                        }


                        var rotatePre3DActor ={
                            actionType:"rotateImage3d",
                            whichAxis:rotateAxis,
                            angleInDegree:rotateAngle
                        }
                        id_NavWindow.modifyActorData("preOprate3DActor",rotatePre3DActor)


                        var rotateProjection ={
                            actionType:"rotateImage3d",
                            whichAxis:rotateAxis,
                            angleInDegree:rotateAngle
                        }

                        id_NavWindow.modifyActorData("projectedCoronal",rotateProjection)
                        id_NavWindow.modifyActorData("projectedSagittal",rotateProjection)


                    }
    }

    onConfirmRegistrationResult:
    {
        console.log("confirm result")
        var newImage3DOrigin = id_NavWindow.getActorParams("preOprate3DActor")["imageOrigin"]
        console.log("newImage3DOrigin",JSON.stringify(newImage3DOrigin))
        var pre3DUltimateMatrix = id_NavWindow.getActorParams("preOprate3DActor")["position"]
        var biasX =  newImage3DOrigin[0]-oldImage3DOrigin[0]
        var biasY =  newImage3DOrigin[1]-oldImage3DOrigin[1]
        var biasZ =  newImage3DOrigin[2]-oldImage3DOrigin[2]
        console.log("pre3DUltimateMatrix before update",JSON.stringify(pre3DUltimateMatrix))
        pre3DUltimateMatrix[3]+=biasX
         pre3DUltimateMatrix[7]+=biasY
         pre3DUltimateMatrix[11]+=biasZ
        console.log("pre3DUltimateMatrix",JSON.stringify(pre3DUltimateMatrix))
        sendResult(pre3DUltimateMatrix)
        removeAssistantTools()

        id_NavWindow.resetStageCamera("id_view3D")


    //        id_posCalculator.getRelativeMatrix
    }

    onGetModelInfomation:
    {
        console.log("get model information")
        console.log("all view model:", JSON.stringify(id_NavWindow.getActorMap()))
        console.log("all stage model:", JSON.stringify(id_NavWindow.getStageActorMap()))
//        console.log("coronal view model:", JSON.stringify(id_view.view2DCoronal.model))

    }

    onSetAllPickStatusFalse:
    {
        setAllPickEventStatusOff()
    }

    onSetPickEventStatus:function changePickStatus(info)
    {
        if(info === "isRGPInProjectedSagittal")
        {
            id_root.boolChooseRGPInProjectedSagittal =true
            turnActorPickableStatusOn("projectedSagittal")
        }
        else if(info === "isRGPInProjectedCoronal")
        {
            id_root.boolChooseRGPInProjectedCoronal =true
            turnActorPickableStatusOn("projectedCoronal")

        }
        else if(info === "isRGPInIntraSagittal")
        {
            id_root.boolChooseRGPInIntraSagittal =true
            turnActorPickableStatusOn("intraSagittalActor")
        }
        else if(info === "isRGPInIntraCoronal")
        {
            id_root.boolChooseRGPInIntraCoronal =true
            turnActorPickableStatusOn("intraCoronalActor")
        }
        else
        {
            console.log("onSetPickEventStatus error")
        }
    }

    onBeginGeneralReg:
    {
         console.log("translation of pre 3D ")
        console.log("translation of pre 3D ", id_posCalculator.get3DModelTranslationOfGeneralReg())
        var changeOriginOfPre3D =
        {
            actionType:"imageOriginTranslation3d",
            translation:id_posCalculator.get3DModelTranslationOfGeneralReg()
        }
        console.log("++++++++++_+_++++++++++0-0")
        id_NavWindow.modifyActorData("projectedCoronal",changeOriginOfPre3D)
//        id_NavWindow.modifyActorData("projectedSagittal",changeOriginOfPre3D)
//        id_NavWindow.modifyActorData("preOprate3DActor",changeOriginOfPre3D)
    }
}


    Component.onCompleted: {
        //设置球的物理半径
        position:id_posCalculator.setNailBallPhysicalRadius(10.5/2)
    }
}
