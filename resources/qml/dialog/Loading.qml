﻿import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Window 2.12
import Fiui 1.0
import ".."
/**@
     filename   Loading.qml
     brief      加载界面
     version    1.0
     author     xh
     date       2021-01-28
     copyright  重庆博仕康科技有限公司
*/

Item{

    id: id_root
    Image {

        source: "qrc:/Finav/image/loadBackground.svg"
        sourceSize.width: parent.width
        sourceSize.height: parent.height
        anchors.centerIn: parent
    }
    FiProgressBar{

        id: id_progressbar
        width: horSpacing(286)
        height: verSpacing(5)
        progressBack: "#818181"
        progressFront: "#4C95F7"
        anchors.centerIn: parent
        anchors.verticalCenterOffset: verSpacing(70)
    }
    Timer{

        id: id_timer
        interval: 20
        running: true
        repeat: true
        onTriggered: _loadingCompleted()
    }
    function _loadingCompleted(){

        if(id_progressbar.progressValue < 1.0){

            id_progressbar.progressValue += 0.01
        }
        else{

            id_timer.stop()
            id_loader.sourceComponent = id_homePage
        }
    }
}

