﻿import QtQuick 2.0
import QtQuick.Window 2.12
import QtQuick.Controls 2.5
import com.json 1.0
import "./panel"
import "./dialog"
/**@
     filename   Main.qml
     brief      主界面
     version    1.0
     author     xh
     date       2021-03-15
     copyright  重庆博仕康科技有限公司
*/
Window{

    id: id_mainWindow
    /**
        @brief  工具包对象
    */
    property var kitsObject: []
    /**
        @brief  手术计划对象
    */
    property var casesObject: []
    /**
        @brief  设备对象
    */
    property var devicesObject: []
    /**
        @brief  病人对象
    */
    property var paientsObject: []
    /**
        @brief  设置对象
    */
    property var settingsObject: []
    /**
        @brief  用户对象
    */
    property var usersObject: []
    property var settings: []
    property point baseSize: "1366,768"
    property real ratioW: 1.0
    property real ratioH: 1.0
    width: Screen.width
    height: Screen.height + 1
    visible: true
    color: "transparent"
    flags: Qt.FramelessWindowHint | Qt.Window | Qt.WindowMinimizeButtonHint | Qt.WindowSystemMenuHint
    GetJSON{

        id: id_json
    }
    Loader{

        id: id_loader
        anchors.centerIn: parent
    }
    Component{

        id: id_homePage
        HomeScreen{

            width: id_mainWindow.width
            height: id_mainWindow.height
        }
    }
    Component{

        id: id_loading
        Loading{

            width: horSpacing(643)
            height: verSpacing(429)
        }
    }
    Component.onCompleted: {

        _setting()
        ratioW = (Screen.width / baseSize.x).toFixed(1)
        ratioH = (Screen.height / baseSize.y).toFixed(1)
        id_loader.sourceComponent = id_loading
    }
    function horSpacing(x){

        return Math.floor(x * ratioW)
    }
    function verSpacing(y){

        return Math.floor(y * ratioH)
    }
    function _setting(){

        kitsObject = id_json.setPath("./settings/modelJson/kits.json")
        casesObject = id_json.setPath("./settings/modelJson/cases.json")
        devicesObject = id_json.setPath("./settings/modelJson/devices.json")
        paientsObject = id_json.setPath("./settings/modelJson/paients.json")
        usersObject = id_json.setPath("./settings/modelJson/users.json")
        settingsObject = id_json.setPath("./settings/modelJson/settings.json")
        settings["kits"] = kitsObject
        settings["cases"] = casesObject
        settings["devices"] = devicesObject
        settings["paients"] = paientsObject
        settings["users"] = usersObject
        settings["settings"] = settingsObject
    }
}
