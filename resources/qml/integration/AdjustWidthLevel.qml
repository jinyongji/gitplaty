﻿import QtQuick 2.0
import Fiui 1.0
/**
    @brief  调整窗宽窗位的滑块
*/
Rectangle{

    /**
        @brief  标签栏
    */
    property string textName
    property alias value: id_widthSlider.sliderValue
    id: id_winWidth
    color: "transparent"
    FiIconButton{

        id: id_widthBtnDel
        anchors.left: parent.left
        anchors.verticalCenter: id_widthSlider.verticalCenter
        width: horSpacing(6)
        height: verSpacing(2)
        btnNormalSrc: "qrc:/Finav/image/sliderdel.svg"
        btnHoverSrc: "qrc:/Finav/image/sliderdel.svg"
        btnPressedSrc: "qrc:/Finav/image/sliderdel.svg"
        onClicked: _sliderDel()
    }
    FiSlider{

        id: id_widthSlider
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.horizontalCenterOffset: horSpacing(-24)
        width: horSpacing(183)
        height: verSpacing(2)
        sliderWidSize: horSpacing(10)
        sliderHeiSize: verSpacing(10)
        sliderStyleSrc: "qrc:/Finav/image/sliderbar.svg"
    }
    FiIconButton{

        id: id_widthBtnAdd
        anchors.left: id_widthSlider.right
        anchors.leftMargin: horSpacing(12)
        anchors.verticalCenter: id_widthSlider.verticalCenter
        width: horSpacing(7)
        height: verSpacing(7)
        btnNormalSrc: "qrc:/Finav/image/slideradd.svg"
        btnHoverSrc: "qrc:/Finav/image/slideradd.svg"
        btnPressedSrc: "qrc:/Finav/image/slideradd.svg"
        onClicked: _sliderAdd()
    }
    FiText{

        id: id_textname
        text: textName
        color: "#A4A4A4"
        font.pixelSize: horSpacing(9)
        anchors.left: id_widthBtnAdd.right
        anchors.leftMargin: horSpacing(12)
        anchors.verticalCenter: id_widthSlider.verticalCenter
    }

    FiText{

        id: id_widthValue
        text: id_widthSlider.sliderValue
        color: "#A4A4A4"
        font.pixelSize: horSpacing(9)
        anchors.left: id_textname.right
        anchors.leftMargin: horSpacing(12)
        anchors.verticalCenter: id_widthSlider.verticalCenter
    }
    function _sliderDel(){

        if(id_widthSlider.sliderValue > id_widthSlider.minValue){

            id_widthSlider.sliderValue -= 1
            id_widthSlider.valueChanged()
        }
        else
            id_widthSlider.sliderValue = id_widthSlider.minValue
    }
    function _sliderAdd(){

        if(id_widthSlider.sliderValue < id_widthSlider.maxValue){

            id_widthSlider.sliderValue += 1
            id_widthSlider.valueChanged()
        }
        else
            id_widthSlider.sliderValue = id_widthSlider.maxValue
    }
}
