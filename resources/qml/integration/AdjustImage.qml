import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Window 2.12
import Fiui 1.0
Item {

    id: id_root
    signal adjust3DImage(var info)
    signal confirmRegistrationResult()
    signal getModelInfomation()
    signal setAllPickStatusFalse()
   signal setPickEventStatus(var info)//打开某一选点事件
    //粗配打点
    property bool isRGPInProjectedCoronal: false
    property bool isRGPInProjectedSagittal: false
    property bool isRGPInIntraCoronal: false
    property bool isRGPInIntraSagittal: false
    signal beginGeneralReg()

    FiButton{

        id: id_RGPInProjectedSagittal
        anchors.top: parent.top
        anchors.topMargin: 30
        width: 100
        height: 40
        text:"投影侧位点"
        onClicked: {
        console.log("投影侧位片特征点")
        setAllPickStatusFalse()
        setPickEventStatus("isRGPInProjectedSagittal")
        }}

    FiButton{

        id: id_RGPInProjectedCoronal
        anchors.top: id_RGPInProjectedSagittal.top
        anchors.left: id_RGPInProjectedSagittal.right
        anchors.leftMargin: 15
        width: 100
        height: 40
        text:"投影正位点"
        onClicked: {
        console.log("投影正位片特征点")
        setAllPickStatusFalse()
        setPickEventStatus("isRGPInProjectedCoronal")
        }}

    FiButton{

        id: id_RGPInIntraSagittal
        anchors.left: id_RGPInProjectedSagittal.left
        anchors.top: id_RGPInProjectedSagittal.bottom
        anchors.topMargin: 15
        width: 100
        height: 40
        text:"术中侧位点"
        onClicked: {
        console.log("术中侧位片特征点")
        setAllPickStatusFalse()
        setPickEventStatus("isRGPInIntraSagittal")
        }}

    FiButton{

        id: id_RGPInIntraCoronal
        anchors.left:id_RGPInIntraSagittal.right
        anchors.top: id_RGPInIntraSagittal.top
        anchors.leftMargin: 15
        width: 100
        height: 40
        text:"术中正位点"
        onClicked: {
        console.log("术中正位片特征点")
        setAllPickStatusFalse()
        setPickEventStatus("isRGPInIntraCoronal")
        }}


    FiButton{

        id: id_generalRegisteConfirm
        anchors.left: id_RGPInProjectedSagittal.left
        anchors.top: id_RGPInIntraSagittal.bottom
        anchors.topMargin: 15
        width: 100
        height: 40
        text:"粗配"
        onClicked: {
        console.log("粗配")
        beginGeneralReg()
        }}



    FiButton{

        id: id_XPlusOne
        anchors.top: id_generalRegisteConfirm.bottom
        anchors.topMargin: 30
        width: 100
        height: 40
        text:"x+1"
        onClicked: {
        console.log("x + 1")
        adjust3DImage("x+1")
        }}

    FiButton{
        id: id_YPlusOne
        width: 100
        height: 40
        anchors.top: id_XPlusOne.bottom
        anchors.topMargin: 5
        text:"y+1"
        onClicked: {
            console.log("y + 1")
            adjust3DImage("y+1")
    }}

    FiButton{
        id: id_ZPlusOne
        width: 100
        height: 40
        anchors.top: id_YPlusOne.bottom
        anchors.topMargin:5
        text:"z+1"
        onClicked:{
            console.log("z+1")
            adjust3DImage("z+1")
    }}

    FiButton{
        id:id_rotateALongXPositive
        width:100
        height:40
        anchors.top: id_ZPlusOne.bottom
        anchors.topMargin:15
        text:"绕x正"
        onClicked:{
            console.log("rotate along x postive")
            adjust3DImage("rotate along x postive")
    }}

    FiButton{
        id:id_rotateAlongYPositive
        width: 100
        height:40
        anchors.top: id_rotateALongXPositive.bottom
        anchors.topMargin: 5
        text:"绕y正"
        onClicked:{
             console.log("rotate along y positive")
            adjust3DImage("rotate along y positive")
    }}

    FiButton{
        id:id_rotateAlongZPositive
        width: 100
        height:40
        anchors.top: id_rotateAlongYPositive.bottom
        anchors.topMargin: 5
        text:"绕z正"
        onClicked:{
            console.log("rotate along z positive")
            adjust3DImage("rotate along z positive")
    }}



    FiButton{

        id: id_XMinusOne
        anchors.top: id_XPlusOne.top
        anchors.left:id_XPlusOne.right
        anchors.leftMargin: 15
        width: 100
        height: 40
        text:"x-1"
        onClicked:{
             console.log("x - 1")
             adjust3DImage("x-1")
    }}

    FiButton{
        id: id_YMinusOne
        width: 100
        height: 40
        anchors.left: id_XMinusOne.left
        anchors.top: id_XMinusOne.bottom
        anchors.topMargin: 5
        text:"y-1"
        onClicked: {
            console.log("y - 1")
            adjust3DImage("y-1")
    }}

    FiButton{
        id: id_ZMinusOne
        width: 100
        height: 40
        anchors.left: id_XMinusOne.left
        anchors.top: id_YMinusOne.bottom
        anchors.topMargin:5
        text:"z-1"
        onClicked:{
            console.log("z-1")
            adjust3DImage("z-1")
    }}

    FiButton{
        id:id_rotateALongXnegative
        width:100
        height:40
        anchors.left: id_XMinusOne.left
        anchors.top: id_ZMinusOne.bottom
        anchors.topMargin:15
        text:"绕x负"
        onClicked:{
            console.log("rotate along x negative")
            adjust3DImage("rotate along x negative")
    }}

    FiButton{
        id:id_rotateAlongYNagative
        width: 100
        height:40
        anchors.left: id_XMinusOne.left
        anchors.top: id_rotateALongXnegative.bottom
        anchors.topMargin: 5
        text:"绕y负"
        onClicked:{
             console.log("rotate along y negative")
            adjust3DImage("rotate along y negative")
    }}

    FiButton{
        id:id_rotateAlongZNagative
        width: 100
        height:40
        anchors.left: id_XMinusOne.left
        anchors.top: id_rotateAlongYNagative.bottom
        anchors.topMargin: 5
        text:"绕z负"
        onClicked: {
            console.log("rotate along z nagative")
            adjust3DImage("rotate along z nagative")
    }}

    FiButton{
        id:id_confirmRegistration
        width: 100
        height:40
        anchors.left: id_XMinusOne.left
        anchors.top: id_rotateAlongZNagative.bottom
        anchors.topMargin: 5
        text:"确认"
        onClicked: {
            confirmRegistrationResult()
            console.log("确认")
    }}

    FiButton{
        id:id_getModelInfomation
        width: 100
        height:40
        anchors.left: id_confirmRegistration.left
        anchors.top: id_confirmRegistration.bottom
        anchors.topMargin: 5
        text:"获取model信息"
        onClicked: {
            getModelInfomation()
            console.log("获取model信息")
    }}

}
