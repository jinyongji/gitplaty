﻿import QtQuick 2.0
import QtQuick.Controls 2.5
import Fiui 1.0
Item {

    id: id_root
//    signal setSpecifiedActorPickableTrue(var witchBotton)
    signal beginAdjustPositon()
    signal rotateAngle(var info)
    signal setAllPickEventStatusfalse()

    signal changePickEventStatus(var info)//打开某一选点

    property bool peakInSagittal: false
    property bool peakInCoronal: false


    FiButton
    {
        id: id_choosePeakInSagittal
        width: 100
        height: 50
        text: "矢状面尖端"
        onClicked:
        {
            setAllPickEventStatusfalse()
            changePickEventStatus("peakInSagittal")
            console.log("choose peak point in sagittal")
        }
    }

    FiButton
    {
        id: id_choosePeakInCoronal
        width: 100
        height: 50
        text: "冠状面尖端"
        anchors.top: id_choosePeakInSagittal.top
        anchors.left:id_choosePeakInSagittal.right
        anchors.leftMargin: 10
        onClicked:
        {
                    setAllPickEventStatusfalse()
            changePickEventStatus("peakInCoronal")
            console.log("choose peak point in coronal")
        }
    }

    FiButton
    {
        id: id_adjustPosition
        width: 100
        height: 50
        text: "调整对齐"
        anchors.top: id_chooseRadiusInSagittal.bottom
        anchors.left:id_choosePeakInSagittal.left
        anchors.topMargin: 30
        onClicked:
        {
            console.log("adjust the initial position")
            beginAdjustPositon()

        }
    }


    Text {
        id: id_rotateSliderText
        text: qsTr("rotateIntra2D")
        anchors.top: id_adjustPosition.bottom
        anchors.left: id_adjustPosition.left
        anchors.topMargin: 5
    }
    FiSlider{

        id: id_rotateSlider
        width: 250
        height: 5
        sliderWidSize: 20
        sliderHeiSize: 20
        minValue: -180
        maxValue: 180
        sliderValue:0
        sliderStyleSrc: "qrc:/Finav/image/sliderbar.svg"
        anchors.left: id_rotateSliderText.left
        anchors.top: id_rotateSliderText.bottom
        anchors.topMargin: 5

        onSliderValueChanged:
        {
           console.log("rotate slider changed",id_rotateSlider.sliderValue)
            rotateAngle(id_rotateSlider.sliderValue)

        }
    }


    FiButton
    {
        id: id_chooseCenterInSagittal
        width: 100
        height: 50
        text: "矢状面圆心"
        visible: false
        anchors.top: id_choosePeakInSagittal.bottom
        anchors.left:id_choosePeakInSagittal.left
        anchors.topMargin: 10
        onClicked:
        {
            console.log("choose center point in sagittal")
        }
    }

    FiButton
    {
        id: id_chooseRadiusInSagittal
        width: 100
        height: 50
        text: "矢状面半径"
        visible: false
        anchors.top: id_chooseCenterInSagittal.bottom
        anchors.left:id_choosePeakInSagittal.left
        anchors.topMargin: 30
        onClicked: console.log("choose radius in sagittal")
    }

    FiButton
    {
        id: id_chooseCenterInCoroanal
        width: 100
        height: 50
        text: "冠状面圆心"
        visible: false
        anchors.top: id_choosePeakInCoronal.bottom
        anchors.left:id_choosePeakInCoronal.left
        anchors.topMargin: 10
        onClicked: console.log("choose center point in coronal")
    }

    FiButton
    {
        id: id_chooseRadiusInCoronal
        width: 100
        height: 50
        text: "冠状面半径"
        visible: false
        anchors.top: id_chooseCenterInCoroanal.bottom
        anchors.left:id_choosePeakInCoronal.left
        anchors.topMargin: 30
        onClicked: console.log("choose radius in coronal")
    }



}
