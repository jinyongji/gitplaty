﻿cmake_minimum_required(VERSION 3.5)

project(finav LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

#set(ITK_DIR "D:\\I_VTK\\ITK\\lib\\cmake\\ITK-5.1")
#set(VTK_DIR "D:\\I_VTK\\VTK\\lib\\cmake\\vtk-8.2")
#set(OpenCV_DIR "D:\\opencv\\x64\\vc16\\lib")
#set(Qt5_DIR "/opt/Qt/Qt5.12.8/5.12.8/gcc_64/qml/Qt")
set(Qt5_DIR "/opt/Qt5.12.8/5.12.8/gcc_64/lib/cmake/Qt5")

find_package( VTK REQUIRED )
include(${VTK_USE_FILE})
find_package( ITK REQUIRED )
include(${ITK_USE_FILE})
find_package(Qt5 COMPONENTS Core Quick Network SerialPort REQUIRED)
find_package( OpenCV REQUIRED )
find_library(NavView_LIB NavView library/lib)
find_library(FIB fib ../fib/bin)
find_library(FIDRI fidri ../fidri/bin)
include(./Fiui.cmake)

add_executable(finav
               main.cpp
               include/global.h
               include/getJSON.h
               include/nailBallRecorgnize.h
               sources/nailBallRecorgnize.cpp
	       resources/Finav.qrc
	       ${FiuiRes})

target_compile_definitions(finav
 PRIVATE $<$<OR:$<CONFIG:Debug>,$<CONFIG:RelWithDebInfo>>:QT_QML_DEBUG>)
target_link_libraries(finav
${NavView_LIB} ${VTK_LIBRARIES} ${ITK_LIBRARIES} ${OpenCV_LIBRARIES} ${FIB} ${FIDRI} Qt5::Core Qt5::Quick Qt5::Network Qt5::SerialPort)
target_include_directories(finav PRIVATE "../fib")
target_include_directories(finav PRIVATE "../fib/lib")
target_include_directories(finav PRIVATE "../fib/nav/include")
target_include_directories(finav PRIVATE "../fidri/include")
